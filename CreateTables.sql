﻿DROP TABLE Projects

CREATE TABLE Projects (
    ID INT PRIMARY KEY,
    Name varchar(256),
    Tag varchar(15),
    PRCode char(8),
    SAPCode char(11),
    PONumber int,
    ChargeCode char(4), -- CapX or OpX
    -- StoryPoints Calculated,
    ProjectDescr varchar(256),
    Phase varchar(15),
    EITPhase varchar(15),
    FundingStatus bit,
    -- Risks -> Subtable
    -- Impacts -> Subtable
    FK_SolutionCaptain varchar(10), -- To Person Table
    -- PO, Delivery Manager, etc -> On 
    FK_BusinessPOC varchar(10), -- To Person table
    Created date,
    LOERequested date,
    -- LOE Delivered -> LOEs Table
    FundingApproved date,
    FundingStart date,
    -- DevelopmentStart -> Delivery Table
    -- DevelopmentEnd -> Delivery Table
    -- DomainStart -> Delivery Table
    -- DomainEnd-> Delivery Table
    -- E2EStart -> Delivery Table
    -- E2EEnd -> Delivery Table
    -- ToProduction -> Delivery Table
    -- GoLive -> Delivery Table
    FundingEnd date,
    -- Comments -> Comments Table
    -- ApprovedBudget -- Roll up?
    -- Expenses -> Actuals Table
    -- RemainingBudget calculated,
    -- ProjectedCost calculated,
    FundingConfidence float
);

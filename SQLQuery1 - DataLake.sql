﻿SELECT * FROM rsd.JiraSummary WHERE DeliveryTeam IN ('(DARK) SCM - Execution','SCM - Execution') ORDER BY JiraKey

SELECT [External Feature ID] from rsd.ACFeature where [Jira Project Key] in ('BSWMSCE','BSWMSCE2') order by  [External Feature ID]

select jiraid, author, id, created, updated, startdate, Timeworked from rsd.JiraWorklog where Etl_Inserted > current_timestamp - 1   

SELECT top 10 * from rsd.ACCapabilityAcceptanceCriteria ORDER BY [FK Capability ID]

select * from rsd.ACCapabilityAcceptanceCriteria where [FK Capability ID] = 14329



SELECT [External Feature ID] from rsd.ACFeature where [Jira Project Key] in ('BSWMSCE','BSWMSCE2') order by  [External Feature ID]

/******* Worklogs test ***/
SELECT ID, JiraID, Author, WorklogBody, Created, StartDate, Timeworked from rsd.JiraWorklog where JiraID in (
SELECT distinct JiraID FROM rsd.JiraSummary WHERE DeliveryTeam IN ('(DARK) SCM - Execution','SCM - Execution') 
AND [Type] ='Sub-Task'
)

/******* Acceptance Criteria ***/
select * from rsd.ACFeatureAcceptanceCriteria 
  where [FK Feature ID] in (13909, 14077, 14078, 14081, 14082, 14083, 14246, 14329, 14332, 14337, 14493, 14494) 
  order by [fk feature id]




  SELECT JiraID, JiraKey, Type, Priority, FeatureJiraKey, ParentJiraID, Created, Creator, Assignee, Severity, CreatorNTID, CreatorEmail, UpdatedOnDate, 
  AssigneeNTID, AssigneeEmail,  DeliveryTeam, Component, Director, Summary, AcceptanceCriteria, Description, AppSupportDirector, SolutionDirector,  SolutionVP,
  PhaseDetected, Channel, EstimatedBuildDate, Status, RemainingEstimate, OriginalEstimate, AggregateTimeSpent, Sprints, Points, Labels, ReleaseProject, ETAForFix 
  FROM rsd.JiraSummary WHERE DeliveryTeam IN ('EDS - Device and Procurement (Brown)','SCM - Device','Products & Services (Savaria)') ORDER BY JiraID

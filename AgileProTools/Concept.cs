﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Concept : ACIssue
    {

        // Concept specific fields go here
        public double _budget;
        public string _activeFlag;
        public string _developmentalStep;
        public string _operationalStep;
        public int _rankGlobal;

        public Concept(int id, string summary, string description, int ownerFK, string tags, ACIssueState state,
            double budget, string activeFlag, string developmentalStep, string operationalStep, int rankGlobal,
            DateTime dateStarted, DateTime dateTarget, DateTime datePortfolioAsk, DateTime dateCreated, DateTime dateLastModified)
        {
            _id = id;
            _summary = summary;
            _description = description;
            _ownerFK = ownerFK;
            _tags = tags;
            _acState = state;
            _budget = budget;
            _activeFlag = activeFlag;
            _developmentalStep = developmentalStep;
            _operationalStep = operationalStep;
            _rankGlobal = rankGlobal;
            _dateStarted = dateStarted;
            _dateTargeted = dateTarget;
            _datePortfolioAsk = datePortfolioAsk;
            _dateCreated = dateCreated;
            _dateModifiedAC = dateLastModified;
            _dateDoR = dateStarted;
            _dateDoD = new DateTime(1900, 1, 1);
            _issueType = ACIssueType.Concept;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Portfolio
    {
        public Dictionary<int, Concept> _concepts;
        public Dictionary<int, ProductBundle> _productBundles;
        public Dictionary<int, Capability> _capabilities;
        public Dictionary<int, Feature> _features;
        public Dictionary<string, Feature> _jiraFeats;
        public Dictionary<int, Story> _stories;
        public Dictionary<int, Task> _tasks;
        public Dictionary<int, User> _usersByACID;
        public Dictionary<string, User> _usersByNTID;
        public List<Worklog> _workLogs;
        public List<Nag> _nags;
        public Dictionary<int, double> _projectPoints;

        private enum HeaderRowType { RowHeader, RowConceptEnd };

        public Portfolio()
        {
            _concepts = new Dictionary<int, Concept>();
            _productBundles = new Dictionary<int, ProductBundle>();
            _capabilities = new Dictionary<int, Capability>();
            _features = new Dictionary<int, Feature>();
            _jiraFeats = new Dictionary<string, Feature>();
            _stories = new Dictionary<int, Story>();
            _tasks = new Dictionary<int, Task>();
            _usersByACID = new Dictionary<int, User>();
            _usersByNTID = new Dictionary<string, User>();
            _workLogs = new List<Worklog>();
            _nags = new List<Nag>();
            _projectPoints = new Dictionary<int, double>();
        }

    /// <summary>
    ///  Add, Stub and Update for Concepts
    /// </summary>
    /// <param name="c"></param>
    public void AddWorkLog(Worklog w)
        {
            _workLogs.Add(w);
        }

        public void AddConcept(Concept c)
        {
            _concepts.Add(c._id, c);
        }

        public void AddConceptStub(int conceptID)
        {
            if (!_concepts.ContainsKey(conceptID))
            {
                _concepts.Add(conceptID, null);
            }
        }

        internal void UpdateConcept(Concept tConcept)
        {
            _concepts[tConcept._id] = tConcept;
        }

        public string GenerateConceptList()
        {
            bool initial = true;
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<int, Concept> kvp in _concepts)
            {
                int key = kvp.Key;
                if (initial)
                {
                    returnString.Append("(" + key.ToString());
                    initial = false;
                }
                else
                {
                    returnString.Append("," + key.ToString());
                }
            }
            if (initial == false)
            {
                returnString.Append(")");
            }
            else
            {
                returnString.Clear();
                returnString.Append("(0)");
            }
            return returnString.ToString();
        }

        public string GenerateSubTaskList()
        {
            bool initial = true;
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<int, Task> kvp in _tasks)
            {
                int key = kvp.Key;
                if (initial)
                {
                    returnString.Append("(" + key.ToString());
                    initial = false;
                }
                else
                {
                    returnString.Append("," + key.ToString());
                }
            }
            if (initial == false)
            {
                returnString.Append(")");
            }
            else
            {
                returnString.Clear();
                returnString.Append("(0)");
            }
            return returnString.ToString();
        }

        /// <summary>
        /// Add, Stub and Update for Product Bundles
        /// </summary>
        /// <param name="pb"></param>
        public void AddProductBundle(ProductBundle pb)
        {
            _productBundles.Add(pb._id, pb);
        }

        public void AddProductBundleStub(int productBundleID)
        {
            if (!_productBundles.ContainsKey(productBundleID))
            {
                _productBundles.Add(productBundleID, null);
            }
        }

        internal void AddNag(Nag.NagVerb id, string desc, string artifact, string owner, string url)
        {
            Nag n = new Nag(id, desc, artifact, owner, url);
            _nags.Add(n);
        }

        internal void UpdateProductBundle(ProductBundle tProductBundle)
        {
            _productBundles[tProductBundle._id] = tProductBundle;
        }

        public string GeneratePBList()
        {
            bool initial = true;
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<int, ProductBundle> kvp in _productBundles)
            {
                int key = kvp.Key;
                if (initial)
                {
                    returnString.Append("(" + key.ToString());
                    initial = false;
                }
                else
                {
                    returnString.Append("," + key.ToString());
                }
            }
            if (initial == false)
            {
                returnString.Append(")");
            }
            else
            {
                returnString.Clear();
                returnString.Append("(0)");
            }
            return returnString.ToString();
        }


        /// <summary>
        /// Add, Stub and Update for Capabilities
        /// </summary>
        /// <param name="c"></param>
        public void AddCapability(Capability c)
        {
            _capabilities.Add(c._id, c);
        }


        public void AddCapabilityStub(int capabilityID)
        {
            if (!_capabilities.ContainsKey(capabilityID))
            {
                _capabilities.Add(capabilityID, null);
            }
        }

        internal void UpdateCapability(Capability tCapability)
        {
            _capabilities[tCapability._id] = tCapability;
        }

        public string GenerateCapabilityList()
        {
            bool initial = true;
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<int, Capability> kvp in _capabilities)
            {
                int key = kvp.Key;
                if (initial)
                {
                    returnString.Append("(" + key.ToString());
                    initial = false;
                }
                else
                {
                    returnString.Append("," + key.ToString());
                }
            }
            if (initial == false)
            {
                returnString.Append(")");
            }
            else
            {
                returnString.Clear();
                returnString.Append("(0)");
            }
            return returnString.ToString();
        }

        /// <summary>
        /// Add, Stub and Update for Features
        /// </summary>
        /// <param name="f"></param>
        public void AddFeature(Feature f)
        {
            try
            {
                _features.Add(f._id, f);

            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            if (f._jiraKey.Length == 0)
            {
                Console.WriteLine("Feature {0} has no Jira equivalent", f._id);
            }
            else
            {
                try
                {
                    _jiraFeats.Add(f._jiraKey, f);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }
        }

        public void AddStory(Story s)
        {
            _stories.Add(s._id, s);
        }
        public void AddTask(Task t)
        {
            _tasks.Add(t._id, t);
        }

        /// <summary>
        /// Add, Stub, Update and GenerateList for User
        /// </summary>
        /// <param name="tUser"></param>
        public void AddUser(User tUser)
        {
            if (tUser._id > 0)
            {
                if (!_usersByACID.ContainsKey(tUser._id))
                {
                    _usersByACID.Add(tUser._id, tUser);
                }
            }
            if (!_usersByNTID.ContainsKey(tUser._ntid.ToUpper()))
            {
                _usersByNTID.Add(tUser._ntid, tUser);
            }
        }

        public void AddUser(string ntid, string name, string email)
        {
            if (_usersByNTID.ContainsKey(ntid))
            {
                return;
            }
            User tUser = new User(name, email, ntid);
            _usersByNTID.Add(tUser._ntid, tUser);
        }

        public void AddUserStub(string ntid)
        {
            if (!_usersByNTID.ContainsKey(ntid))
            {
                _usersByNTID.Add(ntid, null);
            }
        }
        public void AddUserStub(int acid)
        {
            if (!_usersByACID.ContainsKey(acid))
            {
                _usersByACID.Add(acid, null);
            }
        }
        internal void UpdateUser(User tUser)
        {
            if (tUser._id >= 0)
            {
                _usersByACID.Add(tUser._id, tUser);
            }
            _usersByNTID[tUser._ntid] = tUser;
        }

        public string GenerateUserList()
        {
            bool initial = true;
            StringBuilder returnString = new StringBuilder();
            foreach (KeyValuePair<string, User> kvp in _usersByNTID)
            {
                string key = kvp.Key;
                if (initial)
                {
                    returnString.Append("('" + key.ToString());
                    initial = false;
                }
                else
                {
                    returnString.Append("','" + key.ToString());
                }
            }
            returnString.Append("')");
            return returnString.ToString();
        }

        public void InitializeAndRollUpWorklogs()
        {

            foreach (Worklog w in _workLogs)
            {
                if (_tasks.ContainsKey(w._parentID))
                {
                    w._parentLink = _tasks[w._parentID];
                    continue;
                }
                if (_stories.ContainsKey(w._parentID))
                {
                    w._parentLink = _stories[w._parentID];
                    continue;
                }
                if (_features.ContainsKey(w._parentID))
                {
                    w._parentLink = _features[w._parentID];
                    continue;
                }
                Console.WriteLine("Cannot find parent for worklog:" + w._id + " " + w._authorEmail + " " + w._loggedAt);
            }
        }

        private void WriteTableRow(StreamWriter sw, HeaderRowType type, string conceptString = "")
        {
            switch (type)
            {
                case HeaderRowType.RowHeader:
                    sw.WriteLine("<tr><th>Type</th>");
                    sw.WriteLine("<th>ID</th>");
                    sw.WriteLine("<th>AC Link</th>");
                    sw.WriteLine("<th>Jira Link</th>");
                    sw.WriteLine("<th>Summary</th>");
                    sw.WriteLine("<th>Description</th>");
                    sw.WriteLine("<th>Acceptance Criteria</th>");
                    sw.WriteLine("<th>Status</th>");
                    sw.WriteLine("<th>State</th>");
                    sw.WriteLine("<th>Owner</th>");
                    sw.WriteLine("<th>Tags</th>");
                    sw.WriteLine("<th>D-Created</th>");
                    sw.WriteLine("<th>D-Modified</th>");
                    sw.WriteLine("<th>D-Started</th>");
                    sw.WriteLine("<th>D-Target</th>");
                    sw.WriteLine("<th>D-DoR</th>");
                    sw.WriteLine("<th>D-DoD</th>");
                    sw.WriteLine("<td>Jira ID</td>");
                    sw.WriteLine("<td>Jira Component</td>");
                    sw.WriteLine("<td>Original Estimate</td>");
                    sw.WriteLine("</tr>");
                    break;
                case HeaderRowType.RowConceptEnd:
                    sw.WriteLine("<tr bgcolor =\"DDDDDD\"><td>End of Concept {0}</td>", conceptString);
                    sw.WriteLine("<td>ID</td>");
                    sw.WriteLine("<td>AC Link</td>");
                    sw.WriteLine("<td>Jira Link</td>");
                    sw.WriteLine("<td>Summary</td>");
                    sw.WriteLine("<td>Description</td>");
                    sw.WriteLine("<td>Acceptance Criteria</td>");
                    sw.WriteLine("<td>Status</td>");
                    sw.WriteLine("<td>State</td>");
                    sw.WriteLine("<td>Owner</td>");
                    sw.WriteLine("<td>Tags</td>");
                    sw.WriteLine("<td>D-Created</td>");
                    sw.WriteLine("<td>D-Modified</td>");
                    sw.WriteLine("<td>D-Started</td>");
                    sw.WriteLine("<td>D-Target</td>");
                    sw.WriteLine("<td>D-DoR</td>");
                    sw.WriteLine("<td>D-DoD</td>");
                    sw.WriteLine("<td>Jira ID</td>");
                    sw.WriteLine("<td>Jira Component</td>");
                    sw.WriteLine("<td>Original Estimate</td>");
                    sw.WriteLine("</tr>");
                    break;

            }

        }

        internal void SetMarkersByWorklogDateRange(DateTime dStart, DateTime dEnd)
        {
            Feature f = null;
            Story s = null;
            Task t = null;
            foreach (Worklog w in _workLogs)
            {
                if ((w._loggedAt >= dStart) && (w._loggedAt <= dEnd))
                {
                    if (w._parentLink == null)
                    {
                        Console.WriteLine("Worklog has no parent!");
                        break;
                    }

                    // Mark the parent as 1 if it's not already marked
                    if (w._parentLink._marker == 0)
                    {
                        w._parentLink._marker = 1;  // Parent marker = 1 (found and not orphaned, at least for now)
                    }

                    // Walk up the tree
                    switch (w._parentLink._issueType)
                    {
                        case ACIssue.ACIssueType.Task:
                            t = (Task)w._parentLink;
                            if (_stories.ContainsKey(t._parentID))
                            {
                                s = _stories[t._parentID];
                                if (s._marker == 0)
                                {
                                    s._marker = 1;
                                }
                                if (_features.ContainsKey(s._parentID))
                                {
                                    f = _features[s._parentID];
                                    f._marker = 1;
                                }
                                else
                                {
                                    s._marker = 2; // Story is orphaned
                                }
                            }
                            else 
                            {
                                t._marker = 2; // Task is orphaned
                            }
                            break;
                        case ACIssue.ACIssueType.Story:
                            s = (Story)w._parentLink;
                            if (_features.ContainsKey(s._parentID))
                            {
                                f = _features[s._parentID];
                                f._marker = 1;
                            }
                            else
                            {
                                s._marker = 2; // Story is orphaned
                            }
                            break;
                        case ACIssue.ACIssueType.Feature:
                            f = (Feature)w._parentLink;
                            f._marker = 1;
                            break;
                    }
                }
            }
        }

        internal void WriteJiraHierarchy(StreamWriter sw, Dictionary<string, string> configKeys, DateTime startDate, DateTime endDate, bool outputByMarker = false)
        {
            // Write the header
            if (outputByMarker)
            {
                sw.WriteLine("<!DOCTYPE html><html><head><title>AgilePro output for {0} ({1} to {2})</title></head>",
                    configKeys["PROJECTS"], startDate.ToString(), endDate.ToString());
            }
            else
            {
                sw.WriteLine("<!DOCTYPE html><html><head><title>AgilePro output for {0}</title></head>",
                    configKeys["PROJECTS"]);
            }
            sw.WriteLine("<body>");
            sw.WriteLine("<table style=\"width:100%\" border=1>");
            WriteTableRow(sw, HeaderRowType.RowHeader);

            // Write Features
           bool initial = true;
            foreach (KeyValuePair<int, Feature> kvp in _features)
            {
                Feature tFeat = kvp.Value;
                if ((tFeat != null) && (tFeat._marker == 1))
                {
                    // Write this Feature
                    tFeat.Write16R(sw, this, true);
                }
            }

            // Write Orphan Stories
            initial = true;
            foreach (KeyValuePair<int, Story> kvp in _stories)
            {
                Story tStory = kvp.Value;
                if ((tStory != null) && (tStory._marker == 2))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Stories and Bugs --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Story
                    tStory.Write16R(sw, this, true);
                }
            }

            // Write Orphan Tasks
            initial = true;
            foreach (KeyValuePair<int, Task> kvp in _tasks)
            {
                Task tTask = kvp.Value;
                if ((tTask != null) && (tTask._marker == 2))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Tasks --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Task
                    tTask.Write16R(sw, this, true);
                }
            }
            sw.WriteLine("</table></body></html>");
        }

        internal void WriteProjectHierarchy(StreamWriter sw, Dictionary<string, string> configKeys, DateTime startDate, DateTime endDate, bool outputByMarker = false)
        {
            // Write the header
            if (outputByMarker)
            {
                sw.WriteLine("<!DOCTYPE html><html><head><title>AgilePro output for {0} ({1} to {2})</title></head>",
                    configKeys["PROJECTS"], startDate.ToString(), endDate.ToString());
            }
            else
            {
                sw.WriteLine("<!DOCTYPE html><html><head><title>AgilePro output for {0}</title></head>",
                    configKeys["PROJECTS"]);
            }
            sw.WriteLine("<body>");
            sw.WriteLine("<table style=\"width:100%\" border=1>");
            WriteTableRow(sw, HeaderRowType.RowHeader);

            // Write the concepts
            foreach (KeyValuePair<int, Concept> kvp in _concepts)
            {
                // Write this Concept
                Concept tCons = kvp.Value;
                if (tCons != null)
                {
                    tCons.Write16R(sw, this); // Recurses and writes children
                    WriteTableRow(sw, HeaderRowType.RowConceptEnd, tCons._id + "&nbsp;" + tCons._summary);
                }
            }

            // Write Orphan Product Bundles
            bool initial = true;
            foreach (KeyValuePair<int, ProductBundle> kvp in _productBundles)
            {
                ProductBundle tProductBundle = kvp.Value;
                if ((tProductBundle != null) && (tProductBundle._parentID == 0))
                {
                    // Write this Product Bundle
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Product Bundles --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");

                    }
                    tProductBundle.Write16R(sw, this);// Recurses and writes children
                }
            }

            // Write Orphan Capabilties
            initial = true;
            foreach (KeyValuePair<int, Capability> kvp in _capabilities)
            {
                Capability tCap = kvp.Value;
                if ((tCap != null) && (tCap._parentID == 0))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Capabilities --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Capability
                    tCap.Write16R(sw, this); // Recurses and writes children
                }
            }

            // Write Orphan Features
            initial = true;
            foreach (KeyValuePair<int, Feature> kvp in _features)
            {
                Feature tFeat = kvp.Value;
                if ((tFeat != null) && (tFeat._parentID == 0))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Features --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Feature
                    tFeat.Write16R(sw, this);
                }
            }

            // Write Orphan Stories
            initial = true;
            foreach (KeyValuePair<int, Story> kvp in _stories)
            {
                Story tStory = kvp.Value;
                if ((tStory != null) && (tStory._parentID == 0))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Stories --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Feature
                    tStory.Write16R(sw, this);
                }
            }

            // Write Orphan Tasks
            initial = true;
            foreach (KeyValuePair<int, Task> kvp in _tasks)
            {
                Task tTask = kvp.Value;
                if ((tTask != null) && (tTask._parentID == 0))
                {
                    if (initial)
                    {
                        initial = false;
                        sw.WriteLine("<tr bgcolor =\"FFAAAA\"><td>-- Orphan Tasks --</td>");
                        sw.WriteLine("<td>ID</td>");
                        sw.WriteLine("<td>AC Link</td>");
                        sw.WriteLine("<td>Jira Link</td>");
                        sw.WriteLine("<td>Summary</td>");
                        sw.WriteLine("<td>Description</td>");
                        sw.WriteLine("<td>Acceptance Criteria</td>");
                        sw.WriteLine("<td>Status</td>");
                        sw.WriteLine("<td>State</td>");
                        sw.WriteLine("<td>Owner</td>");
                        sw.WriteLine("<td>Tags</td>");
                        sw.WriteLine("<td>D-Created</td>");
                        sw.WriteLine("<td>D-Modified</td>");
                        sw.WriteLine("<td>D-Started</td>");
                        sw.WriteLine("<td>D-Target</td>");
                        sw.WriteLine("<td>D-DoR</td>");
                        sw.WriteLine("<td>D-DoD</td>");
                        sw.WriteLine("<td>Jira ID</td>");
                        sw.WriteLine("<td>Jira Component</td>");
                        sw.WriteLine("<td>Original Estimate</td>");
                        sw.WriteLine("</tr>");
                    }
                    // Write this Feature
                    tTask.Write16R(sw, this);
                }
            }
            sw.WriteLine("</table></body></html>");
        }
        internal void WriteWorklogs(int id, StreamWriter sw)
        {
            // Write all the worklogs corresponding to jiraID = id
            foreach (Worklog w in _workLogs)
            {
                if (w._parentID == id)
                {
                    w.Write16R(sw);
                }
            }
        }

        internal void WriteSprintHealthCheck(StreamWriter sw, Dictionary<string, string> configKeys)
        {
            //Recursively check each feature and its subelements
            foreach (KeyValuePair<int, Feature> kvp in _features)
            {
                Feature f = kvp.Value;
                f.QualityCheck(this, sw);
            }
            foreach (KeyValuePair<int, Story> kvp in _stories)
            {
                Story s = kvp.Value;
                s.QualityCheck(this, sw);
            }
            foreach (KeyValuePair<int, Task> kvp in _tasks)
            {
                Task t = kvp.Value;
                t.QualityCheck(this, sw);
            }

            // TODO - Feature state vs story state, features without stories, features without Capability parents
            // TODO - Stories whose status don't match tasks, Stories with tasks out of sync
            // TODO - stories with only some of the standard tasks present (based on Labels)
            // Todo - tasks with multiple AGP labels
            // TODO - tasks with mangled AGP labels

            Nag.WriteNagHeaderHTML(sw, configKeys["PROJECTS"]);
            foreach (Nag n in _nags)
            {
                n.WriteHTML(sw);
            }
            Nag.WriteNagTrailerHTML(sw);
            sw.WriteLine("</table></body></html>");

            foreach (KeyValuePair<int, double> kvp in _projectPoints)
            {
                Console.WriteLine("Project: {0}, points {1}", kvp.Key, kvp.Value);
            }

        }

        public void AddAllJiraUsers()
        {
            foreach (KeyValuePair<int, Feature> kvp in _features)
            {
                ACIssue item = kvp.Value;
                if ((item._jiraCreatorNTID != null) && (item._jiraCreatorNTID != ""))
                {
                    AddUser(item._jiraCreatorNTID, item._jiraCreator, item._jiraCreatorEmail);
                }
                if ((item._jiraAssigneeNTID != null) && (item._jiraAssigneeNTID != ""))
                {
                    AddUser(item._jiraAssigneeNTID, item._jiraAssignee, item._jiraAssigneeEmail);
                }
            }
            foreach (KeyValuePair<int, Story> kvp in _stories)
            {
                ACIssue item = kvp.Value;
                if ((item._jiraCreatorNTID != null) && (item._jiraCreatorNTID != ""))
                {
                    AddUser(item._jiraCreatorNTID, item._jiraCreator, item._jiraCreatorEmail);
                }
                if ((item._jiraAssigneeNTID != null) && (item._jiraAssigneeNTID != ""))
                {
                    AddUser(item._jiraAssigneeNTID, item._jiraAssignee, item._jiraAssigneeEmail);
                }
            }
            foreach (KeyValuePair<int, Task> kvp in _tasks)
            {
                ACIssue item = kvp.Value;
                if ((item._jiraCreatorNTID != null) && (item._jiraCreatorNTID != ""))
                {
                    AddUser(item._jiraCreatorNTID, item._jiraCreator, item._jiraCreatorEmail);
                }
                if ((item._jiraAssigneeNTID != null) && (item._jiraAssigneeNTID != ""))
                {
                    AddUser(item._jiraAssigneeNTID, item._jiraAssignee, item._jiraAssigneeEmail);
                }
            }

            foreach (Worklog w in _workLogs)
            {
                if ((w._authorEmail != null) && (w._authorEmail != ""))
                {
                    AddUser(w._authorEmail, w._authorEmail, w._authorEmail);
                }
            }

        }

    }
}


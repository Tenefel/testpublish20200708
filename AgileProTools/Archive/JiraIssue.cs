﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AgileProTools
{
    class JiraIssue
    {
        public string _expand;
        public int _id; // Internal ID
        public string _selfRestURL;
        public string _key; //  Identifier in the form <project>-##
        public string _browseURL;
        public string _assigneeEmail;
        public string _assigneeName;
        public string _creatorEmail;
        public string _creatorName;
        public string _labels;
        public string _issueTypeString;
        public enum JiraIssueType { Feature,Story,Task,Bug,Unknown };
        public JiraIssueType _issueType;
        public string _projectKey;
        public string _projectName;
        public string _lastUpdatedDate;
        public string _description;
        public string _summary;
        public string _priority;
        public string _status;
        public string _createdDate;
        public string _featureLink;
        public string _parentLink;
        public bool _isParent = false;
        public bool _isChild = false;
        public List<string> _subtasks = new List<string>();
        public double _originalEstimate; // Original estimate
        public double _remainingEstimate; // Original estimate
        public double _totalLoggedTime;
        public double _totalEstimateTime; // Current estimate (remaining?)
        public List<Worklog> _workLogs;
        public bool __written;
        public enum PhaseState { NotStarted, InProgress, Done };
        public PhaseState _taskStatus;
        public enum AGPStateType { NotStarted=0, Analysis=1, DesignDoc=2, Coding=3, CodeReview=4, DevValidation=5, QAValidation=6, RFA=7, Accepted=8, Unknown=9 };
        public AGPStateType _agpState;  // Used for story categorization for cumulative flow
        public AGPStateType _taskAGPState;   // Used for task categorization during object construction

        public int ID { get { return _id; } }
        public double ProgressPercent() { return _totalLoggedTime * 100.0 / _totalEstimateTime; } // If Estimate == remaining, rework this equation
        public string Key { get { return _key; } }

        /// <summary>
        /// WorkItem - constructor
        /// Author: Steve Ciccarelli
        /// </summary>
        /// <param name="isDefect">True if we are creating a DEFECT workitem, otherwise false (for User Story)</param>
        /// <param name="jToken">The JSON to unpack to create this object</param>
        //public JiraIssue(JToken jToken)
        //{
        //    _workLogs = new List<Worklog>();
        //    _agpState = AGPStateType.Unknown;
        //    _issueType = JiraIssueType.Unknown;

        //    _expand = jToken["expand"].ToString();
        //    _id = (int)DataAccess.UnpackLongInt(jToken["id"].ToString());
        //    _selfRestURL = jToken["self"].ToString();
        //    _key = jToken["key"].ToString();
        //    _originalEstimate = DataAccess.UnpackDouble(jToken["fields"]["aggregatetimeoriginalestimate"].ToString());
        //    _remainingEstimate = DataAccess.UnpackDouble(jToken["fields"]["aggregatetimeestimate"].ToString());
        //    _totalLoggedTime = DataAccess.UnpackDouble(jToken["fields"]["progress"]["progress"].ToString());
        //    _totalEstimateTime = DataAccess.UnpackDouble(jToken["fields"]["progress"]["total"].ToString());
        //    try
        //    {
        //        _assigneeEmail = jToken["fields"]["assignee"]["emailAddress"].ToString();
        //        _assigneeName = jToken["fields"]["assignee"]["displayName"].ToString();
        //    }
        //    catch
        //    {
        //        _assigneeEmail = "";
        //        _assigneeName = "";
        //    }
        //    try
        //    {
        //        _creatorEmail = jToken["fields"]["creator"]["emailAddress"].ToString();
        //        _creatorName = jToken["fields"]["creator"]["displayName"].ToString();
        //    }
        //    catch
        //    {
        //        _assigneeEmail = "";
        //        _assigneeName = "";
        //    }
        //    try
        //    {
        //        _parentLink = jToken["fields"]["parent"]["key"].ToString();
        //    }
        //    catch
        //    {
        //        _parentLink = "";
        //    }
        //    _labels = jToken["fields"]["labels"].ToString();
        //    _labels = _labels.Replace("[\r\n", "").Replace("\r\n]", "").Replace("\r\n", "|").Replace("[]", "").Replace(",|  ", "|").Replace("\"", "");
        //    _taskAGPState = AGPStateType.Unknown;
        //    if (_labels.ToUpper().Contains("AGP:ANALYSISTASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.Analysis;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:DESIGNDOCTASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.DesignDoc;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:CODINGTASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.Coding;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:REVIEWTASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.CodeReview;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:DEVVALIDATETASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.DevValidation;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:QATASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.QAValidation;
        //    }
        //    if (_labels.ToUpper().Contains("AGP:ACCEPTANCETASK") == true)
        //    {
        //        _taskAGPState = AGPStateType.Accepted;
        //    }

        //    _issueTypeString = jToken["fields"]["issuetype"]["name"].ToString();
        //    if (_issueTypeString.CompareTo("Feature") == 0)
        //    {
        //        _issueType = JiraIssueType.Feature;
        //    }
        //    else if (_issueTypeString.CompareTo("Story") == 0)
        //    {
        //        _issueType = JiraIssueType.Story;
        //    }
        //    else if (_issueTypeString.CompareTo("Task") == 0)
        //    {
        //        _issueType = JiraIssueType.Task;
        //    }
        //    else if (_issueTypeString.CompareTo("Sub-task") == 0)
        //    {
        //        _issueType = JiraIssueType.Task;
        //    }
        //    else if (_issueTypeString.CompareTo("Bug") == 0)
        //    {
        //        _issueType = JiraIssueType.Bug;
        //    }
        //    else
        //    {
        //        _issueType = JiraIssueType.Unknown;
        //    }
        //    _projectKey = jToken["fields"]["project"]["key"].ToString();
        //    _projectName = jToken["fields"]["project"]["name"].ToString();
        //    _lastUpdatedDate = jToken["fields"]["updated"].ToString();
        //    _createdDate = jToken["fields"]["created"].ToString();
        //    _summary = jToken["fields"]["summary"].ToString();
        //    _description = jToken["fields"]["description"].ToString().Replace('\n', ' ').Replace('\r', ' ');
        //    _summary = jToken["fields"]["summary"].ToString();
        //    _priority = jToken["fields"]["priority"]["name"].ToString();
        //    _status = jToken["fields"]["status"]["name"].ToString();
        //    _browseURL = "https://jirasw.t-mobile.com/browse/" + _key;
        //    try
        //    {
        //        _featureLink = jToken["fields"]["customfield_10101"].ToString();
        //    }
        //    catch
        //    {
        //        _featureLink = "";
        //    }
        //    string subtaskString = jToken["fields"].ToString();
        //    GetTasks(subtaskString, _subtasks);
        //}

        public string StateToString()
        {
            string returnString = "";
            switch (_agpState)
            {
                case AGPStateType.Accepted:
                    returnString = "Accepted";
                    break;
                case AGPStateType.Analysis:
                    returnString = "Analysis";
                    break;
                case AGPStateType.CodeReview:
                    returnString = "CodeReview";
                    break;
                case AGPStateType.Coding:
                    returnString = "Coding";
                    break;
                case AGPStateType.DesignDoc:
                    returnString = "DesignDoc";
                    break;
                case AGPStateType.DevValidation:
                    returnString = "DevValidation";
                    break;
                case AGPStateType.NotStarted:
                    returnString = "NotStarted";
                    break;
                case AGPStateType.QAValidation:
                    returnString = "QAValidation";
                    break;
                case AGPStateType.RFA:
                    returnString = "Ready for Acceptance";
                    break;
                default:
                case AGPStateType.Unknown:
                    returnString = "Unknown";
                    break;
            }
            return returnString;
        }

        //private List<string> GetTasks(string input, List<string> results)
        //{
        //    results.Clear();
        //    JObject jResults = JObject.Parse(input);
        //    IList<JToken> jResultsItems = jResults["subtasks"].Children().ToList();
        //    for (int i = 0; i < jResultsItems.Count(); i++)
        //    {
        //        results.Add(jResultsItems[i]["key"].ToString());
        //    }
        //    return results;
        //}


        //internal void Write16R(StreamWriter sw, Portfolio port)
        //{
        //    // If we're a feature, don't write this since the ACIssue was already written
        //    if (_issueType != ACIssueType.Feature)
        //    {
        //        sw.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
        //            _issueTypeString,
        //            _id,
        //            _browseURL,
        //            DataAccess.StripNonPrintingChars(_summary),
        //            DataAccess.StripNonPrintingChars(_description), // 4
        //            DataAccess.StripNonPrintingChars(_status),
        //            StateToString(),
        //            _assigneeName, //7
        //            DataAccess.StripNonPrintingChars(_labels),
        //            _createdDate,
        //            _lastUpdatedDate, // 10
        //            "--",
        //            "--",
        //            "--",
        //            "--", // 14
        //            _id);
        //    }

        //    // If we're a feature or a story, write the tasks
        //    if ((_issueType == ACIssueType.Story) || (_issueType == JiraIssueType.Feature))
        //    {

        //        foreach (int taskID in _subtasks)
        //        {
        //            port._tasks[taskID].Write16R(sw, port);

        //        }
        //   }
        //    else if (_issueType == JiraIssueType.Task)
        //    {
        //        // Write out the worklogs, if any
        //        foreach (Worklog w in _workLogs)
        //        {
        //            w.Write16R(sw);
        //        }
        //    }
        //}

    }
}

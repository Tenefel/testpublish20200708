﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Task : ACIssue
    {
        private const string TaskLabel_Analysis = "AGP:ANALYSISTASK";
        private const string TaskLabel_DesignDoc = "AGP:DESIGNDOCTASK";
        private const string TaskLabel_Coding = "AGP:CODINGTASK";
        private const string TaskLabel_CodeReview = "AGP:REVIEWTASK";
        private const string TaskLabel_DevValidate = "AGP:DEVVALIDATETASK";
        private const string TaskLabel_QA = "AGP:QATASK";
        private const string TaskLabel_Acceptance = "AGP:ACCEPTANCETASK";

        private const string TaskLabel_SOXCompliance = "AGP:SOXCOMPLIANCETASK";
        private const string TaskLabel_PerformanceTesting = "AGP:PERFORMANCETESTING";
        private const string TaskLabel_SecurityTesting = "AGP:SECURITYTESTING";

        public enum TaskState { NotStarted, InProgress, Done, Unknown}

        public enum TaskType { None, Analysis, DesignDocUpdate, Coding, CodeReview, 
            DevValidation, QAValidation, Acceptance, SoxCompliance, PerformanceTesting, SecurityTesting}
        public TaskType _taskType;

        public Task(int id, string jiraKey, string status, string priority, int parentID, string summary, string labels,
            DateTime dateCreated, string creator, string assignee, string severity, string creatorNTID, string creatorEmail, DateTime dateModified,
             string assigneeNTID, string assigneeEmail, string deliveryTeam, string component, string director, string acceptanceCriteria,
             string description, string directorAppSupport, string directorSolution, string vp, string phase, DateTime estBuild, double remainingEst,
              double origEst, double aggTimeSpent, string sprint, double storyPoints, string releaseProj, DateTime estFixDate)

        {
            _id = id;
            _jiraKey = jiraKey;
            _issueType = ACIssueType.Task;
            _status = status;
            _priority = priority;
            _parentID = parentID;
            _summary = summary;
            _labels = labels;

            _dateCreated = dateCreated;
            _jiraCreator = creator;
            _jiraAssignee = assignee;
            _jiraSeverity = severity;
            _jiraCreatorNTID = creatorNTID;
            _jiraCreatorEmail = creatorEmail;
  
            _dateModifiedJira = dateModified;
            _jiraAssigneeNTID = assigneeNTID;
            _jiraAssigneeEmail = assigneeEmail;
            _jiraDeliveryTeam = deliveryTeam;
            _jiraComponent = component;
            _jiraDirector = director;

            UpdateAcceptanceCriteria(acceptanceCriteria);
            _description = description;
            _jiraDirectorAppSupport = directorAppSupport;
            _jiraDirectorSolution = directorSolution;
            _jiraVP = vp;
            _jiraPhase = phase;

            _estimatedBuild = estBuild;
            _remainingEstimate = remainingEst;
            _originalEstimate = origEst;
            _rollupWorkLogged = -1;
            _jiraSprints = sprint;
            _storyPoints = storyPoints;
            _jiraReleaseProject = releaseProj;
            _estimatedFixDate = estFixDate;

            CalculateTaskLabel();  // Side effects setting _taskLabelType
        }

        public void QualityCheck(Portfolio port, StreamWriter sw)
        {
            if (this._status.CompareTo("Unknown") == 0)
            {
                return;
            }

            this.QualityCheckACIssue(port, sw);
            CalculateProgressState();

            // Looking at task specific issues
            // New but has no estimate
            if (_originalEstimate == 0)
            {
                port.AddNag(Nag.NagVerb.E_Task_NotEstimated, "Task: " + _jiraKey + " has no original estimate", _jiraKey, _jiraAssignee, JiraUrl());
            }

            if (_status.ToUpper() == "OPEN")
            {
                // New status - do we have hours worked?
                if (_rollupWorkLogged > 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_TodoWithHours, "Task: " + _jiraKey + " is in the ToDo/Open state, but has hours logged against it (should be in progress)", _jiraKey, _jiraAssignee, JiraUrl());
                } 
                // New but has no remaining estimate
                if (_remainingEstimate == 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_ToDoWithNoRemainingTime, "Task: " + _jiraKey + " has no original estimate", _jiraKey, _jiraAssignee, JiraUrl());
                }
            }
            else if (_status.ToUpper() == "IN PROGRESS")
            {
                // In Progress but no time recorded
                if (_rollupWorkLogged == 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_InProgWithZeroActuals, "Task: " + _jiraKey + " is in progress but has no hours logged", _jiraKey, _jiraAssignee, JiraUrl());
                }
                // In Progress but no time remaining
                if (_remainingEstimate == 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_InProgWithZeroRemaining, "Task: " + _jiraKey + " is in progress but has no remaining estimate", _jiraKey, _jiraAssignee, JiraUrl());
                }
            }
            else if (_status.ToUpper() == "DONE")
            {
                // Done but time remaining
                if (_remainingEstimate > 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_CompletedWithRemainingWork, "Task: " + _jiraKey + " is done but has remaining estimated time", _jiraKey, _jiraAssignee, JiraUrl());
                }
                // Done but no work logged
                if (_rollupWorkLogged == 0)
                {
                    port.AddNag(Nag.NagVerb.E_Task_CompletedWithZeroWork, "Task: " + _jiraKey + " is done but has no work logged", _jiraKey, _jiraAssignee, JiraUrl());
                }
            }
        }

        private void CalculateTaskLabel()
        {
            if (_labels.ToUpper().Contains(TaskLabel_Analysis))
            {
                _taskType = TaskType.Analysis;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_DesignDoc))
            {
                _taskType = TaskType.DesignDocUpdate;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_Coding))
            {
                _taskType = TaskType.Coding;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_CodeReview))
            {
                _taskType = TaskType.CodeReview;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_DevValidate))
            {
                _taskType = TaskType.DevValidation;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_QA))
            {
                _taskType = TaskType.QAValidation;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_Acceptance))
            {
                _taskType = TaskType.Acceptance;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_SOXCompliance))
            {
                _taskType = TaskType.SoxCompliance;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_PerformanceTesting))
            {
                _taskType = TaskType.PerformanceTesting;
            }
            else if (_labels.ToUpper().Contains(TaskLabel_SecurityTesting))
            {
                _taskType = TaskType.SecurityTesting;
            }
            else _taskType = TaskType.None;
        }

        public TaskState CalculateProgressState()
        {
            // Progress state is a function of:
            //  _remainingEstimate - if it's zero, we're done
            //  _rollupWorkLogged - if it's zero, we haven't started
            //  Otherwise both _rollupWorkLogged and _remainingEstimate are non-zero, so we're in progress
            if (_remainingEstimate == 0)
            {
                return TaskState.Done;
            }
            if (_rollupWorkLogged == 0)
            {
                return TaskState.NotStarted;
            }
            return TaskState.InProgress;
        }

        public bool isSprintTask()
        {
            if ((_taskType == TaskType.Analysis)
                || (_taskType == TaskType.DesignDocUpdate)
                || (_taskType == TaskType.Coding)
                || (_taskType == TaskType.CodeReview)
                || (_taskType == TaskType.DevValidation)
                || (_taskType == TaskType.QAValidation)
                || (_taskType == TaskType.Acceptance))
            {
                return true;
            }
            return false;
        }

        public bool isReleaseTask()
        {
            if ((_taskType == TaskType.SoxCompliance)
                || (_taskType == TaskType.PerformanceTesting)
                || (_taskType == TaskType.SecurityTesting))
            {
                return true;
            }
            return false;
        }
    }
}

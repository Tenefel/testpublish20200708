﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AgileProTools
{
    class Worklog
    {
        public int _id;
        public int _parentID;
        public string _authorEmail;
        public string _description;
        public DateTime _loggedAt;
        public int _loggedTime;
        public bool _isTaskWorklog;
        public ACIssue _parentLink;

        public Worklog(int id, int jiraID, string authorEmail, string description, DateTime created, int loggedSeconds)
        {
            _id = id;
            _parentID = jiraID;
            _authorEmail = authorEmail;
            _description = description;
            _loggedAt = created;
            _loggedTime = loggedSeconds;
            _isTaskWorklog = false;
            _parentLink = null;
        }

        internal void Write16R(StreamWriter sw)
        {
            string paddingString = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
           string bgColorString = "bgcolor=\"#D0D0D0\"";

            sw.Write("<tr {0}>", bgColorString);
            sw.Write("<td>{0}</td>", paddingString + "Work Log");
            sw.Write(DataAccess.GenerateHtmlTdForString(_id.ToString()));
            sw.Write("<td>--</td>");
            sw.Write("<td>--</td>");
            double hours = _loggedTime / 3600.0;
            sw.Write("<td>{0}</td>", "Logged: " + hours.ToString() + "h"); // Summary
            sw.Write(DataAccess.GenerateHtmlTdForString(DataAccess.StripNonPrintingChars(_description)));  // Description
            sw.Write("<td>--</td>"); // status
            sw.Write("<td>--</td>"); // state
            sw.Write("<td>{0}</td>", _authorEmail); // owner
            sw.Write("<td>--</td>"); // tags
            sw.Write(DataAccess.GenerateHtmlTdForDate(_loggedAt)); // created
            sw.Write("<td>--</td>"); // modified
            sw.Write("<td>--</td>"); // date start
            sw.Write("<td>--</td>"); // date target
            sw.Write("<td>--</td>"); // date ready
            sw.Write("<td>--</td>"); // date done
            sw.Write("<td>--</td>"); // Jira ID
            sw.Write("</tr>\n\n");

        }
    }
}

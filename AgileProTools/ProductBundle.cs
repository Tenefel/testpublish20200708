﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class ProductBundle : ACIssue
    {
        // Product Bundle specific fields go here
        public string _devStep;
        public string _busImpact;
        public string _riskAppetite;
        public string _failureImpact;
        public string _failureProbability;

        /// <summary>
        /// Product Bundle - Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="summary"></param>
        /// <param name="description"></param>
        /// <param name="ownerFK"></param>
        /// <param name="tags"></param>
        public ProductBundle(int id, string name, string description, int ownerFK, string tags, int parent, string devStep, ACIssueState state,
            string businessImpact, string riskAppetite, string itRisk, string failureImpact, string failureProbability, DateTime dateTarget,
            DateTime dateStart, DateTime datePortfolioAsk, DateTime dateCreated, DateTime dateLastModified, DateTime dateInProgress, DateTime dateAccepted) 
        {
            _id = id;
            _summary = name;
            _description = description;
            _ownerFK = ownerFK;
            _tags = tags;
            _devStep = devStep;
            _acState = state;
            _parentID = parent;
            _busImpact = businessImpact;
            _riskAppetite = riskAppetite;
            _failureImpact = failureImpact;
            _failureProbability = failureProbability;
            _dateTargeted = dateTarget;
            _dateStarted = dateStart;
            _datePortfolioAsk = datePortfolioAsk;
            _dateCreated = dateCreated;
            _dateModifiedAC = dateLastModified;
            _dateStarted = dateInProgress;
            _dateDoD = dateAccepted;
            _issueType = ACIssueType.ProductBundle;
        }

    }
}


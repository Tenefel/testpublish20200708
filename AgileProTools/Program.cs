﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AgileProTools
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static Dictionary<string, string> configKeys = new Dictionary<string, string>();
        static string dbName;
        static DateTime noDate = new DateTime(2900, 1, 1);
        enum VerbType { Usage, Encode, Hierarchy, AddTasks, InSprint, AgileCraft, Upload };

        static bool GetCommandParameter(string[] args, string testString, ref string value)
        {
            // Argument is of the format -xxxxx=yyyyy
            // If -xxxxx=yyyyy is passed on the command line, then value will be set to yyyyy and true will be returned
            // Otherwise value is set to "" and false is returned
            for (int i = 0; i < args.Length; i++)
            {
                if (string.Compare(args[i].ToUpper(), 0, testString.ToUpper(), 0, testString.Length) == 0)
                {
                    dbName = args[i].Substring(testString.Length + 1);
                    return true;
                }
            }
            return false;

        }
        static bool GetDBName(string[] args)
        {
            return GetCommandParameter(args, "-DBNAME", ref dbName);
        }

        static string GetLoginInfo()
        {
            Console.Write("Account:");
            string userName = Console.ReadLine().Trim();
            Console.Write("Password:");
            string passWord = Console.ReadLine().Trim();
            string returnString = userName.Trim() + ":" + passWord.Trim();
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(returnString);
            return System.Convert.ToBase64String(plainTextBytes);
        }



       private static VerbType ReadCommandArgs(string[] args, Dictionary<string, string> configKeys)
        {
            VerbType workType = VerbType.Hierarchy;

            for (int i = 0; i < args.Length; i++)
            {
                string key = args[i].ToUpper().Substring(1);
                string value = "YES";
                int pos = args[i].IndexOf('=');
                if (pos > 0)
                {
                    key = args[i].ToUpper().Substring(1, pos - 1);
                    value = args[i].Substring(pos + 1);
                }
                switch (key)
                {
                    case "ADDTASKS":
                        workType = VerbType.AddTasks;
                        break;
                    case "ENCODE":
                        workType = VerbType.Encode;
                        break;
                    case "HIERARCHY":
                        workType = VerbType.Hierarchy;
                        break;
                    case "INSPRINT":
                        workType = VerbType.InSprint;
                        break;
                    case "USE":
                        workType = VerbType.Usage;
                        break;
                    case "AGILECRAFT":
                        workType = VerbType.AgileCraft;
                        break;
                    default:
                        break;
                }
                if (configKeys.ContainsKey(key))
                {
                    configKeys[key] = value;
                }
                else
                {
                    configKeys.Add(key, value);
                }
            }
            return workType;
        }
        
        private static string ReadPassword()
        {
            StringBuilder pwd = new StringBuilder();
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pwd.Append(key.KeyChar);
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pwd.Length > 0)
                    {
                        pwd.Remove(pwd.Length - 1, 1);
                        Console.Write("\b \b");
                    }
                    else if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            }
            return pwd.ToString();
        }

        private static void DoAgilecraftDashboard(Dictionary<string, string> configKeys, string account, string pwd, StreamWriter sw)
        {
            Portfolio port = new Portfolio();
            Console.WriteLine("Portfolio Initialized");

            DataAccess.ReadAgileCraftData(configKeys, account, pwd, port);
            Console.WriteLine("AgileCraft Data Read Completed");

            DataAccess.ReadJiraData(configKeys, account, pwd, port);
            Console.WriteLine("Jira Data Read Completed");

            // For now, dump this to the sw file
            port.WriteProjectHierarchy(sw, configKeys, noDate, noDate);
        }

        private static DateTime GetDate(string prompt)
        {
            Console.Write(prompt + ": ");
            string response = Console.ReadLine();
            DateTime rDate = new DateTime(1900, 1, 1);
            try
            {
                rDate = Convert.ToDateTime(response);
            }
            catch
            {
                rDate = noDate;
            }
            if (rDate.CompareTo(noDate) == 0)
            {
                Console.WriteLine("No " + prompt + " specified.");
            }
            else
            {
                Console.WriteLine(prompt + " entered: " + rDate.ToString());
            }
            return rDate;
        }

        private static VerbType GetActivity(VerbType verb)
        {
            // List all of the possible verbs including the default, let the user pick one and then return the new verb
            Console.WriteLine("Pick an action:");

            // Verbs are InSprint, Hierarchy
            if (verb == VerbType.InSprint)
            {
                Console.Write("(I)nsprint**, ");
            }
else
            {
                Console.Write("(I)nsprint, ");
            }
            if (verb == VerbType.Hierarchy)
            {
                Console.Write("(H)ierarchy** : ");
            }
            else
            {
                Console.Write("(H)ierarchy : ");
            }
            if (verb == VerbType.Upload)
            {
                Console.Write("(U)pload** : ");
            }
            else
            {
                Console.Write("(U)pload : ");
            }


            ConsoleKeyInfo k =      Console.ReadKey();
            if ((k.KeyChar == 'I') || (k.KeyChar == 'i'))
            {
                return VerbType.InSprint;
            }
            if ((k.KeyChar == 'H') || (k.KeyChar == 'h'))
            {
                return VerbType.Hierarchy;
            }
            if ((k.KeyChar == 'U') || (k.KeyChar == 'i'))
            {
                return VerbType.Upload;
            }

            return verb;
        }


        static void Usage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine(" AgilePro <args> where <args> can be:");
            Console.WriteLine("   -Encode : Encodes an account:password combo into base64 (auth token credentials)");
            Console.WriteLine("   -DBName : Specifies the name of the database file to use ");
            Console.WriteLine("   -AuthToken : Specifies an authentication token to use for Jira interaction");
            Console.WriteLine("   -ProjectSet : Specifies a CSV separated set of projects to use for Jira operations");
            Console.WriteLine("   -WriteCSV : Generates a hierarchy of Issues and writes to a CSV file");
            Console.WriteLine("   -ShowWorkLogs : Tells the hierarchy generation logic to include worklogs");
            Console.WriteLine("   -CurrentSprint : Tells the hierarchy generation logic to filter for current sprint items only");
            Console.WriteLine("   -AddTasks:<id> : Adds tasks to the story specified by <id> // Not functional");
            Console.WriteLine("   -InSprint : Runs in-sprint reporting for specified teams");
            Console.WriteLine("   -Upload : Uploads tab delimited files to the database");

            Console.WriteLine("Note: Command switches will override configuration entries in the specified database");
        }
        static void ExitPause()
        {
            Console.WriteLine("Press a key to exit...");
            Console.ReadKey();
        }
        static void Main(string[] args)
        {
            bool exitPause = true;
            string authToken = "";
            string fileName = "c:\\AgilePro_output.txt";
            HashSet<string> features = new HashSet<string>();
            HashSet<string> parents = new HashSet<string>();
            VerbType verb = VerbType.Usage;
            Dictionary<long, Worklog> workLogs = new Dictionary<long, Worklog>();
            StreamWriter sw = null;

            if (args.Length == 0)
            {
                verb = VerbType.Usage;
            }

            // If there is a database name specified in the command line, read configuration from that first
            if (GetDBName(args))
            {
                DataAccess.ReadConfiguration(dbName, configKeys);
            }

            // Override any database configuration values with command line values.
            verb = ReadCommandArgs(args, configKeys);
            if (configKeys.ContainsKey("OUTFILE"))
            {
                fileName = configKeys["OUTFILE"];
            }
            try
            {
                sw = new StreamWriter(fileName);
            }
            catch
            {
                Console.WriteLine("Unable to open output file: {0}", fileName);
                return;
            }

            Console.WriteLine("Started at {0}", DateTime.Now.ToString());
            string pwd;
            string account;

            Portfolio port = new Portfolio();
            Console.WriteLine("Portfolio Initialized");

            verb = GetActivity(verb);

            switch (verb)
            {
                case VerbType.Usage:
                default:
                    Usage();
                    ExitPause();
                    break;  //New comment
                case VerbType.Encode:
                    var base64EncodedBytes = System.Convert.FromBase64String(configKeys["AUTHTOKEN"]);
                    Console.WriteLine(System.Text.Encoding.UTF8.GetString(base64EncodedBytes));
                    Console.WriteLine("Operation: Encode");
                    authToken = GetLoginInfo();
                    Console.WriteLine(authToken);
                    ExitPause();
                    break;
                case VerbType.InSprint:
                    Console.WriteLine("Operation: InSprint work output");
                    Console.Write("Password:");
                    pwd = ReadPassword();
                    Console.WriteLine();
                    Console.WriteLine("Password read");
                    account = configKeys["USEREMAIL"];
                    DataAccess.ReadJiraData(configKeys, account, pwd, port, true);
                    Console.WriteLine("Jira Data Read Completed");

                    // For now, dump this to the sw file
                    port.WriteSprintHealthCheck(sw, configKeys);
                    sw.Close();
                    break;
                case VerbType.Hierarchy:
                    Console.WriteLine("Operation: Date Range Hierarchy output selected");

                    // Read the password (hidden)
                    Console.Write("Password:");
                    pwd = ReadPassword();
                    Console.WriteLine();
                    Console.WriteLine("Password read");

                    DateTime dStart = GetDate("Start Date");
                    DateTime dEnd = GetDate("End Date");
                    if (dEnd.CompareTo(noDate) == 0)
                    {
                        dEnd = new DateTime(2222, 1, 1);
                    }

                    account = configKeys["USEREMAIL"];
                    DataAccess.ReadJiraData(configKeys, account, pwd, port, true);
                    Console.WriteLine("Jira Data Read Completed");

                    // Set the ACIssue._marker field for all hierarchial ACIssues based on WorkLog date range
                    port.SetMarkersByWorklogDateRange(dStart, dEnd);

                    // For now, dump this to the sw file
                    port.WriteJiraHierarchy(sw, configKeys, dStart, dEnd, true);
                    sw.Close();
                    break;

                  case VerbType.Upload:
                    
                    break;
                  /* Agilecraft fetch and merge is offline 3/10/2020
                     * 
                case VerbType.AgileCraft:
                    Console.Write("Password:");
                    pwd = ReadPassword();
                    Console.WriteLine();
                    Console.WriteLine("Password read");
                    account = configKeys["USEREMAIL"];
                    DoAgilecraftDashboard(configKeys, account, pwd, sw);
                    sw.Close();
                    break;
                    */

            }
            Console.WriteLine("Completed at {0}", DateTime.Now.ToString());
            Console.WriteLine("CSV file written: {0} ", fileName);
            if (exitPause)
            {
                ExitPause();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class User
    {
        public int _id;
        public string _email;
        public string _firstName;
        public string _lastName;
        public string _fullName;
        public string _extID;
        public string _title;
        public string _region;
        public string _city;
        public bool _isActive;
        public string _ntid;

        List<string> _nags;

        public User(int id, string firstName, string lastName, string fullName, string email, string extID, string title, string region, string city, bool isActive, string ntid)
        {
            _id = id;
            _email = email;
            _firstName = firstName;
            _lastName = lastName;
            _fullName = fullName;
            _extID = extID;
            _title = title;
            _region = region;
            _city = city;
            _isActive = isActive;
            _ntid = ntid;
            _nags = new List<string>();
        }

        public User(string fullName, string email, string ntid)
        {
            _id = 0;
            _email = email;
            _firstName = null;
            _lastName = null;
            _fullName = fullName;
            _extID = null;
            _title = null;
            _region = null;
            _city = null;
            _isActive = true;
            _ntid = ntid;
            _nags = new List<string>();
        }

        public void UpdateUser(string ntid, string email, string fullName)
        {
            _email = email;
            _fullName = fullName;
            _ntid = ntid;
        }
    }
}

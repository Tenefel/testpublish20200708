﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Story : ACIssue
    {
        private const string ReleaseStoryLabel = "AGP:RELEASESTORY";

        public enum StoryState { Unknown, New, Draft, DoR, Sprint_ActiveWork, Sprint_Analysis, Sprint_DesignDoc, Sprint_Coding, Sprint_CodeReview, Sprint_DevValidation, Sprint_QAValidation, Sprint_Acceptance, Sprint_Complete, InPostSprintTesting, ReleaseReady, Released, Retired }
        public StoryState _storyState;

        public bool _isReleaseStory;
        public int _standardTaskCount;
        public int _incompleteTaskCount;
        public bool _tasksOutOfOrder;
        public Story(int id, string jiraKey, string jiraFeatureKey, string status, string priority, int parentID, string summary, string labels,
            DateTime dateCreated, string creator, string assignee, string severity, string creatorNTID, string creatorEmail, DateTime dateModified,
             string assigneeNTID, string assigneeEmail, string deliveryTeam, string component, string director, string acceptanceCriteria,
             string description, string directorAppSupport, string directorSolution, string vp, string phase, DateTime estBuild, double remainingEst,
              double origEst, double aggTimeSpent, string sprint, double storyPoints, string releaseProj, DateTime estFixDate, string issueType)

        {
            _id = id;
            _jiraKey = jiraKey;
            _jiraFeatureKey = jiraFeatureKey;
            if (issueType.ToUpper().CompareTo("BUG") != 0)
            {
                _issueType = ACIssueType.Story;
            }
            else
            {
                _issueType = ACIssueType.Bug;
            }
            _status = status;
            _priority = priority;
            _parentID = parentID;
            _summary = summary;
            _labels = labels;
            SetStoryState(); // Has to be done after the labels are read

            _dateCreated = dateCreated;
            _jiraCreator = creator;
            _jiraAssignee = assignee;
            _jiraSeverity = severity;
            _jiraCreatorNTID = creatorNTID;
            _jiraCreatorEmail = creatorEmail;

            _dateModifiedJira = dateModified;
            _jiraAssigneeNTID = assigneeNTID;
            _jiraAssigneeEmail = assigneeEmail;
            _jiraDeliveryTeam = deliveryTeam;
            _jiraComponent = component;
            _jiraDirector = director;
            UpdateAcceptanceCriteria(acceptanceCriteria);
            _description = description;
            _jiraDirectorAppSupport = directorAppSupport;
            _jiraDirectorSolution = directorSolution;
            _jiraVP = vp;
            _jiraPhase = phase;

            _estimatedBuild = estBuild;
            _remainingEstimate = remainingEst;
            _originalEstimate = origEst;
            _rollupWorkLogged = aggTimeSpent; // TODO:  to be rolled up from work items when accessible
            _jiraSprints = sprint;
            _storyPoints = storyPoints;
            _jiraReleaseProject = releaseProj;
            _estimatedFixDate = estFixDate;

            _isReleaseStory = IsReleaseStory();
            _standardTaskCount = 0 ;
         _incompleteTaskCount = 0;
            _tasksOutOfOrder = false;
    }

    private bool IsReleaseStory()
        {
            foreach (Task t in _childObjects)
            {
                if (t.isReleaseTask())
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsFunctionalStory()
        {
            foreach (Task t in _childObjects)
            {
                if (t.isSprintTask())
                {
                    return true;
                }
            }
            return false;
        }


        private void SetStoryState()
        {
            _storyState = StoryState.Unknown;
            if (_status.ToUpper().CompareTo("NEW") == 0)
            {
                _storyState = StoryState.New;
                if (_labels.ToUpper().Contains("SCE:DRAFTREADY"))
                {
                    _storyState = StoryState.Draft;
                }
            }
            else if (_status.ToUpper().CompareTo("DEFINED") == 0)
            {
                _storyState = StoryState.DoR;
            }
            else if (_status.ToUpper().CompareTo("IN PROGRESS") == 0)
            {
                // Return value is an in sprint state, or Unknown if the story's tasks don't indicate that it's
                //    actively being worked on
                StoryState inSprintState = CalculateInSprintStateFromTasks();
                if (inSprintState != StoryState.Unknown)
                {
                    // Story is actively being worked on
                    _storyState = inSprintState;
                }
                else
                {
                    // Not actively being worked on
                    if (_incompleteTaskCount == 0)
                    {
                        _storyState = StoryState.Sprint_ActiveWork;
                    }
                    else
                    {
                        _storyState = StoryState.Sprint_Complete;
                    }
                }
            }
            else if (_status.ToUpper().CompareTo("DEV COMPLETE") == 0)
            {
                _storyState = StoryState.Sprint_Complete;
                if (_labels.ToUpper().Contains("SCE:RELEASETESTINGUNDERWAY"))
                {
                    _storyState = StoryState.InPostSprintTesting;
                }
                
            }
            else if (_status.ToUpper().CompareTo("TEST COMPLETE") == 0)
            {
                _storyState = StoryState.ReleaseReady;
            }
            else if (_status.ToUpper().CompareTo("DONE") == 0)
            {
                _storyState = StoryState.Released;
                if (_labels.ToUpper().Contains("SCE:RETIRED"))
                {
                    _storyState = StoryState.Retired;
                }
            }
            else if (_status.ToUpper().CompareTo("WITHDRAWN") == 0)
            {
                _storyState = StoryState.Retired;
            }
        }

        private StoryState CalculateInSprintStateFromTasks()
        {
            int analysisTasks = 0;
            int analysisStarted = 0;
            int analysisDone = 0;
            int designDocTasks = 0;
            int designDocStarted = 0;
            int designDocDone = 0;
            int codingTasks = 0;
            int codingStarted = 0;
            int codingDone = 0;
            int codeReviewTasks = 0;
            int codeReviewStarted = 0;
            int codeReviewDone = 0;
            int devValidateTasks = 0;
            int devValidateStarted = 0;
            int devValidateDone = 0;
            int qaValidateTasks = 0;
            int qaValidateStarted = 0;
            int qaValidateDone = 0;
            int acceptanceTasks = 0;
            int acceptanceStarted = 0;
            int acceptanceDone = 0;

            _incompleteTaskCount = 0;

            foreach (ACIssue issue in _childObjects)
            {
                Task t = (Task)issue;
                Task.TaskState ts = t.CalculateProgressState();

                if (t._taskType == Task.TaskType.Analysis)
                {
                    analysisTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        analysisStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        analysisDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.DesignDocUpdate)
                {
                    designDocTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        designDocStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        designDocDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.Coding)
                {
                    codingTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        codingStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        codingDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.CodeReview)
                {
                    codeReviewTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        codeReviewStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        codeReviewDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.DevValidation)
                {
                    devValidateTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        devValidateStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        devValidateDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.QAValidation)
                {
                    qaValidateTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        qaValidateStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        qaValidateDone++;
                    }
                }
                else if (t._taskType == Task.TaskType.Acceptance)
                {
                    acceptanceTasks++;
                    if (ts == Task.TaskState.InProgress)
                    {
                        acceptanceStarted++;
                    }
                    else if (ts == Task.TaskState.Done)
                    {
                        acceptanceDone++;
                    }
                }

                // Regardless of task type, is this task incomplete?
                if ((ts == Task.TaskState.NotStarted) || (ts == Task.TaskState.InProgress))
                {
                    _incompleteTaskCount++;
                }

            }

            _standardTaskCount = 0;
            if (analysisTasks > 0) _standardTaskCount++;
            if (designDocTasks > 0) _standardTaskCount++;
            if (codingTasks > 0) _standardTaskCount++;
            if (codeReviewTasks > 0) _standardTaskCount++;
            if (devValidateTasks > 0) _standardTaskCount++;
            if (qaValidateTasks > 0) _standardTaskCount++;
            if (acceptanceTasks > 0) _standardTaskCount++;

            if ((analysisTasks > 0) && (analysisStarted > 0))
            {
                if (((designDocTasks > 0) && (designDocStarted > 0))
                     || ((codingTasks > 0) && (codingStarted > 0))
                     || ((codeReviewTasks > 0) && (codeReviewStarted > 0))
                     || ((devValidateTasks > 0) && (devValidateStarted > 0))
                     || ((qaValidateTasks > 0) && (qaValidateStarted > 0))
                     || ((acceptanceTasks > 0) && (acceptanceStarted > 0)))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_Analysis;
            }
            if ((designDocTasks > 0) && (designDocStarted > 0))
            {
                if (((codingTasks > 0) && (codingStarted > 0))
                     || ((codeReviewTasks > 0) && (codeReviewStarted > 0))
                     || ((devValidateTasks > 0) && (devValidateStarted > 0))
                     || ((qaValidateTasks > 0) && (qaValidateStarted > 0))
                     || ((acceptanceTasks > 0) && (acceptanceStarted > 0)))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_DesignDoc;
            }
            if ((codingTasks > 0) && (codingStarted > 0))
            {
                if (((codeReviewTasks > 0) && (codeReviewStarted > 0))
                    || ((devValidateTasks > 0) && (devValidateStarted > 0))
                    || ((qaValidateTasks > 0) && (qaValidateStarted > 0))
                    || ((acceptanceTasks > 0) && (acceptanceStarted > 0)))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_Coding;
            }
            if ((codeReviewTasks > 0) && (codeReviewStarted > 0))
            {
                if (((devValidateTasks > 0) && (devValidateStarted > 0))
                    || ((qaValidateTasks > 0) && (qaValidateStarted > 0))
                    || ((acceptanceTasks > 0) && (acceptanceStarted > 0)))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_CodeReview;
            }
            if ((devValidateTasks > 0) && (devValidateStarted > 0))
            {
                if (((qaValidateTasks > 0) && (qaValidateStarted > 0))
                   || ((acceptanceTasks > 0) && (acceptanceStarted > 0)))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_DevValidation;
            }
            if ((qaValidateTasks > 0) && (qaValidateStarted > 0))
            {
                if ((acceptanceTasks > 0) && (acceptanceStarted > 0))
                {
                    _tasksOutOfOrder = true;
                }
                return StoryState.Sprint_QAValidation;
            }
            if ((acceptanceTasks > 0) && (acceptanceStarted > 0))
            {
                return StoryState.Sprint_Acceptance;
            }

            // Otherwise, we're not in an in-sprint story state, return Unknown
            return StoryState.Unknown;
        }

        public void QualityCheck(Portfolio port, StreamWriter sw)
        {
            if (this._status.CompareTo("Unknown") == 0)
            {
                return;
            }

            this.QualityCheckACIssue(port, sw);

            bool checkForTaskExistence = false;
            // Quality check tasks, task status
            switch (_storyState)
            {
                case StoryState.New:
                    // OK to not have tasks, acceptance criteria
                    break;
                case StoryState.Draft:
                    break;
                case StoryState.DoR:
                    checkForTaskExistence = true;
                        break;
                case StoryState.Sprint_Analysis:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_DesignDoc:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_Coding:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_CodeReview:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_DevValidation:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_ActiveWork:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_QAValidation:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Sprint_Acceptance:
                    checkForTaskExistence = true;
                    break;
                case StoryState.InPostSprintTesting:
                    checkForTaskExistence = true;
                    break;
                case StoryState.ReleaseReady:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Released:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Retired:
                    checkForTaskExistence = true;
                    break;
                case StoryState.Unknown:
                    checkForTaskExistence = true;
                    port.AddNag(Nag.NagVerb.E_Story_NoStoryStatus, _issueType.ToString() + ": " + _jiraKey + " has no recognizable workflow status.", _jiraKey, _jiraAssignee, JiraUrl());
                    break;

            }

            if ((checkForTaskExistence) && (_childObjects.Count == 0))
            {
                port.AddNag(Nag.NagVerb.E_Story_NoTasks, _issueType.ToString() + ": " + _jiraKey + " has no tasks.", _jiraKey, _jiraAssignee, JiraUrl());
            }

            if ((_standardTaskCount>0) &&(_standardTaskCount < 6))
            {
                port.AddNag(Nag.NagVerb.E_Story_TaskExecutionOutOfOrder, _issueType.ToString() + ": " + _jiraKey + " has standard tasks, but they're executing out of order.", _jiraKey, _jiraAssignee, JiraUrl());
            }

            if (_tasksOutOfOrder)
            {
                port.AddNag(Nag.NagVerb.E_Story_NotAllStandardTasksPresent, _issueType.ToString() + ": " + _jiraKey + " has an incomplete set of standard tasks.", _jiraKey, _jiraAssignee, JiraUrl());
            }

            /*
            ,  // Artifact does not have at least one of each standard task
            */
        }

    }
}

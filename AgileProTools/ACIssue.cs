﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AgileProTools
{
    class ACIssue
    {
        public int _id;
        public int _jiraRowID;
        public string _jiraKey;
        public enum ACIssueType { Concept, ProductBundle, Capability, Feature, Story, Bug, Task, Undefined };
        public ACIssueType _issueType;
        public string _status;
        public string _priority;
        public string _jiraFeatureKey;
        public int _parentID;
        public DateTime _dateCreated;
        public string _jiraCreator;
        public string _jiraAssignee;
        public int _ownerFK;
        public string _jiraSeverity;
        public string _jiraCreatorNTID;
        public string _jiraCreatorEmail;
        public DateTime _dateModifiedAC; // Jira: UpdatedOnDate
        public DateTime _dateModifiedJira; // Jira: UpdatedOnDate
        public string _jiraAssigneeNTID;
        public string _jiraAssigneeEmail;
        public string _jiraDeliveryTeam;
        public string _jiraComponent;
        public string _jiraDirector;
        public string _summary;
        public string _acceptanceCriteria;
        public string _description;
        public string _jiraDirectorAppSupport;
        public string _jiraDirectorSolution;
        public string _jiraVP;
        public string _jiraPhase;
        public DateTime _estimatedBuild;
        public double _remainingEstimate;
        public double _originalEstimate;
        public double _rollupWorkLogged; // TODO: Roll up the work logged
        public string _jiraSprints;
        public double _storyPoints;
        public string _tags;
        public string _labels;    // Jira labels
        public string _jiraReleaseProject;
        public DateTime _estimatedFixDate;
        public bool _jiraOnly;

        public HashSet<int> _children;
        public List<ACIssue> _childObjects;
        public List<Worklog> _workLogs;

        // ACIssueState applies to Concept, Product Bundle, Capability, Feature
        public enum ACIssueState { New, InIntake, Funded, DefinitionOfReady, DefinitionOfDone, Unknown }
        public ACIssueState _acState;
        public DateTime _dateDoR;
        public DateTime _dateDoD;
        public DateTime _datePortfolioAsk;
        public DateTime _dateTargeted;
        public DateTime _dateStarted;

        public string NoPriorString = "--";

        public int _marker;


        public ACIssue()
        {
            _id = -1;
            _jiraKey = "";
            _jiraRowID = -1;
            _status = "Unknown";
            _priority = "";
            _jiraFeatureKey = "";
            _parentID = 0;
            _summary = "";
            _jiraCreator = "";
            _jiraAssignee = "";
            _ownerFK = -1;
            _jiraSeverity = "";
            _jiraCreatorNTID = "";
            _jiraCreatorEmail = "";
            _jiraAssigneeNTID = "";
            _jiraAssigneeEmail = "";
            _jiraDeliveryTeam = "";
            _jiraComponent = "";
            _jiraDirector = "";
            _acceptanceCriteria = NoPriorString;
            _description = "";
            _jiraDirectorAppSupport = "";
            _jiraDirectorSolution = "";
            _jiraVP = "";
            _jiraPhase = "";
            _remainingEstimate = -1.0;
            _originalEstimate = -1.0;
            _rollupWorkLogged = 0;
            _jiraSprints = "";
            _storyPoints = -1;
            _tags = "";
            _labels = "";
            _jiraReleaseProject = "";

            _jiraOnly = false;
            _acState = ACIssueState.Unknown;
            _issueType = ACIssueType.Undefined;

            _dateDoR = new DateTime(1900, 1, 1);
            _dateDoD = new DateTime(1900, 1, 1);
            _datePortfolioAsk = new DateTime(1900, 1, 1);
            _dateTargeted = new DateTime(1900, 1, 1);
            _dateStarted = new DateTime(1900, 1, 1);
            _estimatedFixDate = new DateTime(1900, 1, 1);
            _estimatedBuild = new DateTime(1900, 1, 1);
            _dateModifiedAC = new DateTime(1900, 1, 1);
            _dateModifiedJira = new DateTime(1900, 1, 1);
            _dateCreated = new DateTime(1900, 1, 1);

            _children = new HashSet<int>();
            _childObjects = new List<ACIssue>();
            _workLogs = new List<Worklog>();

            _marker = 0;
        }

        internal void UpdateAcceptanceCriteria(string acceptanceCriteria)
        {
            if (_acceptanceCriteria.Equals(NoPriorString)
                && acceptanceCriteria != null
                && !acceptanceCriteria.Equals(""))
            {
                _acceptanceCriteria = acceptanceCriteria;
            }
        }
        public string TypeToString()
        {
            string returnString = "";
            switch (_issueType)
            {
                case ACIssueType.Concept:
                    returnString = "Concept";
                    break;
                case ACIssueType.ProductBundle:
                    returnString = "Product Bundle";
                    break;
                case ACIssueType.Capability:
                    returnString = "Capability";
                    break;
                case ACIssueType.Feature:
                    returnString = "Feature";
                    break;
                case ACIssueType.Story:
                    returnString = "Story";
                    break;
                case ACIssueType.Bug:
                    returnString = "Bug";
                    break;
                case ACIssueType.Task:
                    returnString = "Task";
                    break;
                default:
                    returnString = "Undefined";
                    break;
            }
            return returnString;
        }

        public void AddChild(int childID)
        {
            _children.Add(childID);
        }

        public void AddChildObject(ACIssue child)
        {
            _childObjects.Add(child);
        }

        public void AddWorklog(Worklog w)
        {
            _workLogs.Add(w);
        }

        public static ACIssueState StringToState(string issueStateString)
        {
            /*
             * 0 - Pending Approval = ACIssueState.New
             * Not Mapped = InIntake
             * Not Mapped = Funded
             * 1 - Ready To Start = DefinitionOfReady
             * 2 - In Progress = Not Mapped
             * 3 - Dev Complete = Not Mapped
             * 4 - Test Complete = Not Mapped
             * 5 - Accepted = DefinitionOfDone
             * */

            ACIssueState tState = ACIssueState.New;
            //Input is one of:  0 - Pending Approval, 1 - Ready To Start, 2 - In Progress, 3 - Dev Complete, 4 - Test Complete, 5 - Accepted
            switch (issueStateString[0])
            {
                case '0':
                    tState = ACIssueState.New;
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                    tState = ACIssueState.DefinitionOfReady;
                    break;
                case '5':
                    tState = ACIssueState.DefinitionOfReady;
                    break;
                default:
                    tState = ACIssueState.New;
                    break;
            }
            return tState;
        }

        public string StateToString()
        {
            string returnString = "Unknown";
            switch (_acState)
            {
                case ACIssueState.New:
                    returnString = "New";
                    break;
                case ACIssueState.InIntake:
                    returnString = "Intake";
                    break;
                case ACIssueState.Funded:
                    returnString = "Funded";
                    break;
                case ACIssueState.DefinitionOfReady:
                    returnString = "At DoR";
                    break;
                case ACIssueState.DefinitionOfDone:
                    returnString = "At DoD";
                    break;
                default:
                    returnString = "Unknown";
                    break;

            }
            return returnString;
        }

        public string ACUrl()
        {
            string returnString = "";
            switch (_issueType)
            {
                case ACIssueType.Concept:
                    returnString = "https://newtmobile.agilecraft.com/ThemeGrid?FirstTime=True&ThemeID=" + _id.ToString();
                    break;
                case ACIssueType.ProductBundle:
                    returnString = "https://newtmobile.agilecraft.com/EpicGrid?FirstTime=True&EpicID=" + _id.ToString();
                    break;
                case ACIssueType.Capability:
                    returnString = "https://newtmobile.agilecraft.com/CapabilitiesGrid?FirstTime=True&CapabilityID=" + _id.ToString();
                    break;
                case ACIssueType.Feature:
                    if (_jiraOnly)
                    {
                        returnString = "--";
                    }
                    else
                    {
                        returnString = "https://newtmobile.agilecraft.com/RequestGrid?FirstTime=True&RequestID=" + _id.ToString();
                    }
                    break;
                case ACIssueType.Story:
                case ACIssueType.Task:
                    returnString = "--";
                    break;
                default:
                    returnString = "--";
                    break;
            }
            return returnString;
        }

        public string JiraUrl()
        {
            string returnString = "";
            switch (_issueType)
            {
                case ACIssueType.Concept:
                case ACIssueType.ProductBundle:
                case ACIssueType.Capability:
                    returnString = "--";
                    break;
                case ACIssueType.Feature:
                case ACIssueType.Story:
                case ACIssueType.Bug:
                case ACIssueType.Task:
                    returnString = "https://jirasw.t-mobile.com/browse/" + _jiraKey;
                    break;
                default:
                    returnString = "--";
                    break;
            }
            return returnString;
        }


        internal void Write16R(StreamWriter sw, Portfolio port, bool writeMarkerOnly=false)
        {
            if (writeMarkerOnly && (this._marker > 0) && (_status.CompareTo("Withdrawn") != 0))
            {

                string ownerString = null;
                string paddingString = "";
                string bgColorString = "";
                switch (_issueType)
                {
                    case ACIssueType.Task:
                        paddingString += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"#F0F0F0\"";
                        break;
                    case ACIssueType.Story:
                        paddingString += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"#F0E0FF\"";
                        break;
                    case ACIssueType.Bug:
                        paddingString += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"#F0E0FF\"";
                        break;
                    case ACIssueType.Feature:
                        paddingString += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"F0F0FF\"";
                        break;
                    case ACIssueType.Capability:
                        paddingString += "&nbsp;&nbsp;&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"FFFFF0\"";
                        break;
                    case ACIssueType.ProductBundle:
                        paddingString += "&nbsp;&nbsp;";
                        bgColorString = "bgcolor=\"FFF0E0\"";
                        break;
                    case ACIssueType.Concept:
                        paddingString = "";
                        bgColorString = "bgcolor=\"FFF0F0\"";
                        break;
                    case ACIssueType.Undefined:
                        break;
                }
                if (_issueType != ACIssueType.Undefined)
                {
                    try
                    {
                        ownerString = port._usersByACID[_ownerFK]._fullName;
                    }
                    catch
                    {
                        ownerString = null;
                    }

                    if (ownerString == null)
                    {
                        ownerString = _jiraAssignee;
                    }

                    sw.Write("<tr {0}>", bgColorString);
                    sw.Write("<td>{0}</td>", paddingString + TypeToString());
                    sw.Write(DataAccess.GenerateHtmlTdForString(_id.ToString()));
                    sw.Write("<td><a href=\"" + ACUrl() + "\" target=\"_blank\">Align Link</a></td>");
                    sw.Write("<td><a href=\"" + JiraUrl() + "\" target=\"_blank\">{0}</a></td>", _jiraKey);
                    sw.Write(DataAccess.GenerateHtmlTdForString(DataAccess.StripNonPrintingChars(_summary)));
                    sw.Write(DataAccess.GenerateHtmlTdForString(DataAccess.StripNonPrintingChars(_description)));
                    sw.Write(DataAccess.GenerateHtmlTdForString(DataAccess.StripNonPrintingChars(_acceptanceCriteria)));
                    sw.Write(DataAccess.GenerateHtmlTdForString(_status));

                    bool statusDone = false;
                    if (_status.CompareTo("Done") == 0)
                    {
                        statusDone = true;
                    }
                    bool statusDevComplete = false;
                    if (_status.CompareTo("Dev Complete") == 0)
                    {
                        statusDevComplete = true;
                    }
                    bool statusOpen = false;
                    if (_status.CompareTo("Open") == 0)
                    {
                        statusOpen = true;
                    }

                    sw.Write(DataAccess.GenerateHtmlTdForString(StateToString()));

                    sw.Write(DataAccess.GenerateHtmlTdForString(ownerString));
                    sw.Write(DataAccess.GenerateHtmlTdForString(DataAccess.StripNonPrintingChars(_tags + "<br>" + _labels).Trim()));

                    bool atDoR = false;
                    if (StateToString().CompareTo("At DoR") == 0)
                    {
                        atDoR = true;
                    }
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateCreated, atDoR | statusDone | statusDevComplete | statusOpen));
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateModifiedAC, atDoR | statusDone));
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateStarted, atDoR | statusDone | statusDevComplete | statusOpen));
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateTargeted));
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateDoR, atDoR | statusDone));
                    sw.Write(DataAccess.GenerateHtmlTdForDate(_dateDoD, statusDone));
                    sw.Write(DataAccess.GenerateHtmlTdForString(_jiraKey));
                    sw.Write(DataAccess.GenerateHtmlTdForString(_jiraComponent));
                    sw.Write(DataAccess.GenerateHoursFromSeconds(_originalEstimate));
                    sw.Write("</tr>\n\n");

                    foreach (ACIssue child in _childObjects)
                    {
                        child.Write16R(sw, port, writeMarkerOnly);
                    }

                        port.WriteWorklogs(_id, sw);
                }
            }
        }

        public bool IsBadJiraString(string s)
        {
            if ((s is null) || (s == "") || (s.StartsWith("Unknown")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void QualityCheckACIssue(Portfolio port, StreamWriter sw)
        {
            if (this._status.CompareTo("Unknown") == 0)
            {
                return;
            }

            this._jiraOnly = false;  // Mark that we've been here
            string issueType = this.TypeToString();

            if (this.IsBadJiraString(this._priority))
            {
                port.AddNag(Nag.NagVerb.E_Issue_NoPriority, TypeToString() + ": "+ _jiraKey + " does not have a priority.", _jiraKey, _jiraAssignee, JiraUrl());
            }
            if (this.IsBadJiraString(this._jiraCreator))
            {
                port.AddNag(Nag.NagVerb.E_Issue_NoCreator, TypeToString() + ": " + _jiraKey + " does not have a creator.", _jiraKey, _jiraAssignee, JiraUrl());
            }
            if (this.IsBadJiraString(this._jiraAssignee))
            {
                port.AddNag(Nag.NagVerb.E_Issue_NoAssignee, TypeToString() + ": " + _jiraKey + " does not have an assignee.", _jiraKey, _jiraAssignee, JiraUrl());
            }
            if (this.IsBadJiraString(this._acceptanceCriteria))
            {
                port.AddNag(Nag.NagVerb.E_Issue_NoAcceptanceCriteria, TypeToString() + ": " + _jiraKey + " does not have acceptance criteria.", _jiraKey, _jiraAssignee, JiraUrl());
            }

            // Tasks are immune to description checks
            if ((this._issueType!=ACIssueType.Task) && (this.IsBadJiraString(this._description)))
            {
                port.AddNag(Nag.NagVerb.E_Issue_NoDescription, TypeToString() + ": " + _jiraKey + " does not have a description.", _jiraKey, _jiraAssignee, JiraUrl());
            }
        }
    }
}

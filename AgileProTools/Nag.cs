﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Nag
    {

        public NagVerb _id;
        public string _desc;
        public string _artifact;
        public string _owner;
        public string _url;

        public enum NagVerb
        {
            E_Issue_NoDescription   // Artifact does not have a description
            , E_Issue_NoOwner       // Artifact does not have an owner
            , E_Issue_NoAcceptance  // Artifact does not have acceptance criteria
            , E_Issue_NoPriority    // Artifact does not have a priority
            , E_Issue_NoCreator
            , E_Issue_NoAssignee
            , E_Issue_NoAcceptanceCriteria
            , E_Issue_NoPhase
            , E_Issue_IsOrphaned
            , E_Feature_NoTrackerTag  // Feature has no tracker tag
            , E_Story_NoTasks  // Story has no tasks
            , E_Story_NoPoints  // Story has no story points
            , E_Story_NoFeature  // Story has no feature link (orphaned)
            , E_Story_NoStoryStatus  // Story has no recognizable workflow state
            , E_Story_TaskExecutionOutOfOrder  // Story has standard tasks but they're not being executed in order
            , E_Feature_NoCapability  // Feature has no capability link (orphaned)
            , E_Story_NotAllStandardTasksPresent // Artifact does not have at least one of each standard task
            , E_Story_AcceptedWithoutPrereqs // Story is accepted without all prior tasks complete
            , E_Story_QAWithoutPrereqs // Story is in qa phase without all prior tasks complete
            , E_Story_DevValidateWithoutPrereqs // Story is in dev validatoin without all prior tasks complete
            , E_Story_CodeReviewWithoutPrereqs // Story is in dev validatoin without all prior tasks complete
            , E_Story_CodingWithoutPrereqs // Story is in dev validatoin without all prior tasks complete
            , E_Story_DesignDocWithoutPrereqs // Story is in dev validatoin without all prior tasks complete
            , E_Task_NotEstimated  // Task has no estimated hours
            , E_Task_TodoWithHours  // Task is not started but has work recorded against it
            , E_Task_TodoDoesntMatchEstimated  // Task is not started but has work recorded against it
            , E_Task_ToDoWithNoRemainingTime
            , E_Task_InProgWithZeroActuals  // Task is started but no worklog
            , E_Task_InProgWithZeroRemaining  // Task is started but has no remaining hours
            , E_Task_CompletedWithZeroWork // Task is completed but has no logged time
            , E_Task_CompletedWithRemainingWork // Task is completed remaining estimate is > 0
            , E_User_WIPViolation // Story is in dev validation without all prior tasks complete
            , I_Feature_StoryPointsTotal // Summarize the story points for this feature
            , I_Feature_CountOfStoriesWithPoints // Count of child stories with story points
            , I_Feature_CountOfStoriesWithoutPoints // Count of child stories without story points
            , I_Feature_AverageStoryPointsPerStory // Average size of child stories with points
            , E_Nag_FinalEnum
        };

        public Nag(NagVerb id, string desc, string artifact, string owner, string url)
        {
            _id = id;
            _desc = desc;
            _artifact = artifact;
            _owner = owner;
            _url = url;
        }

        public static void WriteNagTrailerHTML(StreamWriter sw)
        {
            sw.WriteLine("</table></body></html>");
        }

        public static void WriteNagHeaderHTML(StreamWriter sw, string teams)
        {
            sw.WriteLine("<!DOCTYPE html><html><head><title>Sprint Hygiene output for {0}</title></head>", teams);
            sw.WriteLine("<body>");
            sw.WriteLine("<table style=\"width:100%\" border=1>");
            sw.WriteLine("<tr><th>ID</th><th>Assignee</th><th>Jira Key</th><th>Description</th><th>URL</th></tr>");
        }

        internal void WriteHTML(StreamWriter sw)
        {
            sw.WriteLine("<tr><td>" + _id + "</td><td>" + _owner + "</td><td>" + _artifact + "</td><td>" + _desc + "</td><td>" + _url + "</td></tr>");
        }
    }
}

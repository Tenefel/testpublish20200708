﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Capability : ACIssue
    {
        // Capability specific fields go here
        public string _extID;
        public double _budget;
        public double _totCapX;
        public double _extCapX;
        public double _totOpX;
        public double _extOpX;
        public DateTime _dateInProgress;

        public Capability(int id, string summary, string description, int ownerFK, string tags, int parent, ACIssueState state, string extID, double budget,
            double totCapX, double extCapX, double totOpX, double extOpX, DateTime dateTargeted, DateTime dateStarted, DateTime datePortfolioAsk,
            DateTime dateCreated, DateTime dateLastModified, DateTime dateInProgress, DateTime dateAccepted)
        {
            _id = id;
            _summary = summary;
            _description = description;
            _ownerFK = ownerFK;
            _tags = tags;
            _parentID = parent;
            _extID = extID;
            _budget = budget;
            _totCapX = totCapX;
            _extCapX = extCapX;
            _totOpX = totOpX;
            _extOpX = extOpX;
            _dateTargeted = dateTargeted;
            _dateStarted = dateStarted;
            _datePortfolioAsk = datePortfolioAsk;
            _dateCreated = dateCreated;
            _dateModifiedAC = dateLastModified;
            _dateInProgress = dateInProgress;
            _dateDoD = dateAccepted;
            _issueType = ACIssueType.Capability;
        }



        /*
         * All Capability Fields:
         * 
         * USED
         * Capability ID
         * Capability Name
         * Capability Description
         * FK User Owner ID
         * Capability State
         * External Capability ID
         * Budget
         * Total CapEx
         * External CapEx
         * Total OpEx
         * External OpEx
         * Date Targeted
         * Date Started
         * Date Portfolio Ask
         * Date Created
         * Date Last Modified
         * Date In Progress
         * Date Accepted
         * 
         * 
         * UNUSED
         * FK Product Bundle ID
         * FK Concept ID
         * dimAC_CapabilityId
         * Man Week Budget
         * Developmental Step
         * Operational Step
         * Capability Type
         * Split Flag
         * Strategic Driver
         * Investment Type
         * Value Scale
         * Kano Complexity
         * Kano Certainty
         * Kano Category
         * MVP Flag
         * Capitalized Flag
         * Rank
         * Rank Global
         * Strategy Goal
         * Quadrant
         * Discount Rate
         * Initial Investment
         * Efficiency Dividend
         * Revenue Assurance
         * Return on Investment
         * Forecast Years
         * Business Impact
         * Risk Appetite
         * IT Risk
         * Failure Impact
         * Failure Probability
         * Parent Goal
         * Strategic Horizon
         * Ability To Execute
         * Spend To Date
         * Estimate At Completion
         * Report Color
         * FK Split Capability ID
         * FK Delivery Group ID
         * FK Work Code ID
         * Custom Text 1
         * Custom Text 2
         * Custom TextArea 1
         * Custom Dropdown 1
         * Custom Dropdown 2
         * Custom Dropdown 3
         * FK Custom Dropdown List 4
         * FK Custom Dropdown List 5
         * FK Tag List ID
         * FK Customer List ID
         * Etl_Inserted
         * Etl_Updated
         * Etl_Deleted
         * */

    }
}

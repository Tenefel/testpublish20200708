﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProTools
{
    class Feature : ACIssue
    {
        // Feature specific fields go here
        public string _capFlag;
        public string _blocked;
        public string _tShirt;
        public string _jiraProjName;
        public DateTime _datePendingApproval;
        public DateTime _dateReadyToStart;
        public DateTime _dateInProgress;
        public DateTime _dateDevComplete;
        public DateTime _dateTestComplete;

        public Feature(int id, string name, string desc, string jiraID, ACIssueState state, string capFlag,
            string blockedFlag, string tShirt, double points, string jiraProjName,
            DateTime dateCreated, DateTime dateModified, DateTime datePendingApproval, DateTime dateDoR,
            DateTime dateInProgress, DateTime dateDevComplete, DateTime dateTestComplete, DateTime dateAccepted,
            DateTime datePortfolioAsk, DateTime dateStart, DateTime dateTargeted, string tags, int ownerFK, int capabilityID)
        {
            _id = id;
            _summary = name;
            _description = desc;
            _ownerFK = ownerFK;
            _tags = tags;
            _parentID = capabilityID;
            _jiraKey = jiraID;
            _capFlag = capFlag;
            _blocked = blockedFlag;
            _tShirt = tShirt;
            _storyPoints = points;
            _jiraProjName = jiraProjName;
            _dateCreated = dateCreated;
            _dateModifiedAC = dateModified;
            _datePendingApproval = datePendingApproval;
            _dateReadyToStart = dateDoR;
            _dateInProgress = dateInProgress;
            _dateDevComplete = dateDevComplete;
            _dateTestComplete = dateTestComplete;
            _dateDoD = dateAccepted;
            _datePortfolioAsk = datePortfolioAsk;
            _dateStarted = dateDoR;
            _dateTargeted = dateTargeted;
            _issueType = ACIssueType.Feature;
        }

        public void UpdateFeatureFromJira(string priority, string featureKey, string creator, string creatorNTID, string creatorEmail,
            string severity,  string assignee, string assigneeNTID, string assigneeEmail, string deliveryTeam, string component, 
            string director, string acceptanceCriteria,string description, string directorAppSupport, string directorSolution, string vp,
             string phase, DateTime estimatedBuild,double remainingEstimate, double originalEstimate, double timeSpent, string sprints,
            double storyPoints, string labels, string releaseProject, DateTime estimatedFix,     DateTime lastModified )
        {
            _priority = priority;
            _jiraFeatureKey = featureKey;
            _jiraCreator = creator;
            _jiraAssignee = assignee;
            _jiraSeverity = severity;
            _jiraCreatorNTID = creatorNTID;
            _jiraCreatorEmail = creatorEmail;
            _dateModifiedJira = lastModified;
            _jiraAssigneeNTID = assigneeNTID;
            _jiraAssigneeEmail = assigneeEmail;
            _jiraDeliveryTeam = deliveryTeam;
            _jiraComponent = component;
            _jiraDirector = director;
            UpdateAcceptanceCriteria(acceptanceCriteria);
            _description = description;
            _jiraDirectorAppSupport = directorAppSupport;
            _jiraDirectorSolution = directorSolution;
            _jiraVP = vp;
            _jiraPhase = phase;
            _estimatedBuild = estimatedBuild;
            _remainingEstimate = remainingEstimate;
            _originalEstimate = originalEstimate;
            _rollupWorkLogged = timeSpent; // TODO:  to be rolled up from work items when accessible
            _jiraSprints = sprints;
            _storyPoints = storyPoints;
            _labels = labels;
            _jiraReleaseProject = releaseProject;
            _estimatedFixDate = estimatedFix;
        }

        internal void QualityCheck(Portfolio port, StreamWriter sw)
        {
            if (this._status.CompareTo("Unknown") == 0)
            {
                return;
            }

            this.QualityCheckACIssue(port, sw);

            // Check for "SCE:xxxxx-#" labels
            string[] labels = _labels.Split(';');
            bool projectLabelFound = false;
            int projectNumber = -1;
            foreach (string s in labels)
            {
                projectNumber = QualityCheckSCETrackerLabel();
                if (projectNumber >= 0)
                {
                    projectLabelFound = true;
                }
            }

            if (!projectLabelFound)
            {
                port.AddNag(Nag.NagVerb.E_Feature_NoTrackerTag, "Add SCE:TrackerTag-## label in Jira for " + _jiraKey + " please.",  _jiraKey, _jiraAssignee, JiraUrl());
            }
            else
            {

                /* Collect data for these info-nags:
                 * , I_Feature_StoryPointsTotal // Summarize the story points for this feature
                 * , I_Feature_CountOfStoriesWithPoints // Count of child stories with story points
                 * , I_Feature_CountOfStoriesWithoutPoints // Count of child stories without story points
                 * , I_Feature_AverageStoryPointsPerStory // Average size of child stories with points
                 */

                int pointedStoryCount = 0;
                int unpointedStoryCount = 0;
                double averageStoryPoints = 0.0;
                foreach (ACIssue tIssue in _childObjects)
                {
                    if (tIssue._issueType == ACIssueType.Story)
                    {
                        if (tIssue._storyPoints > 0)
                        {
                            pointedStoryCount++;
                            averageStoryPoints += tIssue._storyPoints;
                        }
                        else
                        {
                            unpointedStoryCount++;
                        }
                    }
                }
                averageStoryPoints /= pointedStoryCount;
                double adjustedStoryPointTotal = averageStoryPoints * (pointedStoryCount + unpointedStoryCount);

                // Sum points for this project
                if (port._projectPoints.ContainsKey(projectNumber))
                {
                    port._projectPoints[projectNumber] += adjustedStoryPointTotal;
                }
                else
                {
                    port._projectPoints.Add(projectNumber, adjustedStoryPointTotal);
                }
                port.AddNag(Nag.NagVerb.I_Feature_StoryPointsTotal, "Feature: " + _jiraKey + " Adjusted story point total: " + ((averageStoryPoints)*(pointedStoryCount+unpointedStoryCount)).ToString() + " Project #: " + projectNumber.ToString(), _jiraKey, _jiraAssignee, JiraUrl());
                port.AddNag(Nag.NagVerb.I_Feature_CountOfStoriesWithPoints, "Feature: " + _jiraKey + " Count of stories with points: " + pointedStoryCount.ToString(), _jiraKey, _jiraAssignee, JiraUrl());
                port.AddNag(Nag.NagVerb.I_Feature_CountOfStoriesWithoutPoints, "Feature: " + _jiraKey + " Count of stories with no points: " + unpointedStoryCount.ToString(), _jiraKey, _jiraAssignee, JiraUrl());
                port.AddNag(Nag.NagVerb.I_Feature_AverageStoryPointsPerStory, "Feature: " + _jiraKey + " Average story point/story: " + averageStoryPoints.ToString(), _jiraKey, _jiraAssignee, JiraUrl());
            }

        }

        private int  QualityCheckSCETrackerLabel()
        {
            // Matching "SCE:[0-9][a-z][A-Z]-#
            int pos = _labels.IndexOf("SCE:");
            if (pos < 0)
            {
                return -1;
            }
            pos += 5;
            pos = _labels.IndexOf('-', pos);
            if (pos < 0)
            {
                return -1;
            }
            pos++;
            if (char.IsDigit(_labels[pos]) )
            {
                int len = 0;
                while ((pos+len < _labels.Length) && (char.IsDigit(_labels[pos+len])))
                {
                    len++;
                }
                return Int32.Parse(_labels.Substring(pos, len));
            }
            return -1;
        }

        /* 
* All Feature Fields
* 
* 
* USED
* Feature ID
* Feature Name
* Feature Description
* External Feature ID
* Feature State
* Capitalized Flag
* Blocked Flag
* T-Shirt Estimate
* Estimated Points
* Feature Budget
* Feature CapEx
* Feature OpeEx
* External CapEx
* External OpEx
* Jira Project Key
* Jira Project Name
* Date Created
* Date Last Modified
* Date Pending Approval
* Date Ready To Start
* Date In Progress
* Date Dev Complete
* Date Test Complete
* Date Accepted
* Date Portfolio Ask
* Date Start
* Date Target Completion
* Tag List
* FK Capability ID
* 
* 
* UNUSED
* dimAC_FeatureId
* Feature Category
* Feature Driver
* Approach
* Feature Type
* Feature Delivery Group Rank
* Rank Delivery Group
* Rank Planning Increment
* Rank Portfolio Planning Increment
* Rank Global
* Rank Product Bundle
* Functional Area
* Source
* MMF Flag
* Benefits
* Member Weeks
* WSJF B Value
* WSJF T Value
* WSJF R Value
* WSJF S Value
* WSJF Value
* Ticket
* Revenue Growth
* Expense Savings
* Priority
* Feature Discount Rate
* Feature Initial Investment
* Business Impact
* Risk Appetite
* IT Risk
* Failure Impact
* Failure Probability
* Spend To Date
* Estimate At Completion
* Report Color
* Developmental Step
* Operational Step
* Split Flag
* FK Planning Increment ID
* FK Concept ID
* FK Product Bundle ID
* FK Delivery Group ID
* FK Application ID
* FK Sprint Target Start ID
* FK Sprint Target End ID
* FK User Owner ID
* FK Work Code ID
* FK Feature Split ID
* Deleted Flag
* Custom Text 1
* Custom Text 2
* Custom TextArea 1
* Custom Dropdown 1
* Custom Dropdown 2
* Custom Dropdown 3
* FK Custom Dropdown List 4
* FK Custom Dropdown List 5
* FK Tag List ID
* FK Customer List ID
* Etl_Inserted
* Etl_Updated
* Etl_Deleted
* * */

        }
    }
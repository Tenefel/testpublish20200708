﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace AgileProTools
{
    class DataAccess
    {
        public static long UnpackLongInt(string input)
        {
            try
            {
                return Convert.ToInt64(input);
            }
            catch
            {

            }
            return 0;
        }

        public static double UnpackDouble(string input)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch
            {

            }
            return 0;
        }

        public static bool UnpackBool(string input)
        {
            try
            {
                if ((input[0].CompareTo('T') == 0) ||
                    (input[0].CompareTo('t') == 0) ||
                    (input[0].CompareTo('Y') == 0) ||
                    (input[0].CompareTo('y') == 0))

                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        public static DateTime UnpackDateTime(string input)
        {
            // Input format: yyyy-mm-ddThh:mm:ss.sssZ 
            // Length is at least 20
            if (input.Length < 20)
            {
                return new DateTime(1900, 1, 1);
            }

            int year = 0;
            bool success = int.TryParse(input.Substring(0, 4), out year);
            if (success)
            {
                int month = 0;
                success = int.TryParse(input.Substring(5, 2), out month);
                if (success)
                {
                    int day = 0;
                    success = int.TryParse(input.Substring(8, 2), out day);
                    if (success)
                    {
                        int hour = 0;
                        success = int.TryParse(input.Substring(11, 2), out hour);
                        if (success)
                        {
                            int min = 0;
                            success = int.TryParse(input.Substring(14, 2), out min);
                            if (success)
                            {
                                int sec = 0;
                                success = int.TryParse(input.Substring(17, 2), out sec);
                                if (success)
                                {
                                    return new DateTime(year, month, day, hour, min, sec);
                                }
                            }
                        }
                    }
                }
            }
            return new DateTime(1900, 1, 1);
        }

        internal static string PrettyPrintDate(DateTime tDate)
        {
            string outString = "--";
            if (tDate.Year!= 1900)
            {
                outString = tDate.ToString();
            }
            return outString;
        }

        public static void ReadConfiguration(string dbName, Dictionary<string, string> configKeys)
        {
            string dbConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=""" + dbName
                                       + @""";Integrated Security=True;Connect Timeout=30";
            using (SqlConnection sqlConn = new SqlConnection(dbConnString))
            {

                try
                {
                    sqlConn.Open();
                    SqlDataReader myReader = null;
                    using (SqlCommand myCommand = new SqlCommand("SELECT ItemKey, ItemValue, Active FROM Configuration"))
                    {

                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            string key = myReader["ItemKey"].ToString().ToUpper();
                            string value = myReader["ItemValue"].ToString();
                            string active = myReader["Active"].ToString();
                            if (active.ToUpper().StartsWith("Y"))
                            {
                                configKeys.Add(key, value);
                            }
                        }
                        myReader.Close();
                    }
                    sqlConn.Close();
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        public static void ReadAgileCraftData(Dictionary<string, string> configKeys, string account, string pwd, Portfolio port)
        {
            string dbConnString = @"Data Source=devsecops-dev.database.windows.net;Initial Catalog=devteqimpwu2sdlcstore-sqldb0;Persist Security Info=True;User ID="+ account + ";Password=" + pwd + @";Authentication=""Active Directory Password"";Connect Timeout=300";
            List<Tuple<int, int>> capabilityFeatureKids = new List<Tuple<int, int>>();
            List<Tuple<int, int>> pbCapabilityKids = new List<Tuple<int, int>>();
            List<Tuple<int, int>> conceptPBKids = new List<Tuple<int, int>>();

            if (!configKeys.ContainsKey("TEAMS"))
            {
                Console.WriteLine("TEAMS key not specified.  Please rerun and specify this key in the format '<jiraproj1>'[,<jiraproj2>]*");
                return;
            }

            Console.WriteLine("Reading Features at: {0}", DateTime.Now.ToString());

            int itemCount = 0;
            using (SqlConnection sqlConn = new SqlConnection(dbConnString))
            {
                try
                {
                    sqlConn.Open();
                    SqlDataReader myReader = null;

                    ///////////////////////////////////////////////////////////////////////////////
                    // Fetch Features based on configKeys["TEAMS"]
                    ///////////////////////////////////////////////////////////////////////////////
                    string cmdString = "SELECT DISTINCT [Feature ID],[Feature Name],[Feature Description],[External Feature ID],[Feature State]," +
                        "[Capitalized Flag],[Blocked Flag],[T-Shirt Estimate],[Estimated Points],[Jira Project Name],[Date Created],[Date Last Modified]," +
                        "[Date Pending Approval],[Date Ready To Start],[Date In Progress],[Date Dev Complete],[Date Test Complete],[Date Accepted]," +
                        "[Date Portfolio Ask],[Date Start],[Date Target Completion],[FK User Owner ID],[FK Capability ID]," +
                        "rsd.ACTagList.[Tag List]" +
                        "FROM rsd.ACFeature LEFT JOIN rsd.ACTagList " +
                        "ON rsd.ACFeature.[FK Tag List ID] = rsd.ACTagList.[FK Tag List ID] " +
                        "WHERE [Jira Project Key] in (" + configKeys["TEAMS"] + ")";

                    // Read all the features and record their Concepts, Product Bundles, Capabilities and Users
                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;
                            // Read Feature Fields
                            int id = (int)UnpackLongInt(myReader["Feature ID"].ToString());
                            string name = myReader["Feature Name"].ToString();
                            string desc = myReader["Feature Description"].ToString();
                            string jiraID = myReader["External Feature ID"].ToString();
                            if (jiraID.CompareTo("Unknown") == 0)
                            {
                                jiraID = "";
                            }
                            ACIssue.ACIssueState state = ACIssue.StringToState(myReader["Feature State"].ToString());
                            string capFlag = myReader["Capitalized Flag"].ToString();
                            string blockedFlag = myReader["Blocked Flag"].ToString();
                            string tShirt = myReader["T-Shirt Estimate"].ToString();
                            int estimatePoints = (int)UnpackLongInt(myReader["Estimated Points"].ToString());
                            string jiraProjectName = myReader["Jira Project Name"].ToString();
                            DateTime dateCreated = UnpackDateTime(myReader["Date Created"].ToString());
                            DateTime dateLastModified = UnpackDateTime(myReader["Date Last Modified"].ToString());
                            DateTime datePendingApproval = UnpackDateTime(myReader["Date Pending Approval"].ToString());
                            DateTime dateReadyToStart = UnpackDateTime(myReader["Date Ready To Start"].ToString());
                            DateTime dateInProgress = UnpackDateTime(myReader["Date In Progress"].ToString());
                            DateTime dateDevComplete = UnpackDateTime(myReader["Date Dev Complete"].ToString());
                            DateTime dateTestComplete = UnpackDateTime(myReader["Date Test Complete"].ToString());
                            DateTime dateAccepted = UnpackDateTime(myReader["Date Accepted"].ToString());
                            DateTime datePortfolioAsk = UnpackDateTime(myReader["Date Portfolio Ask"].ToString());
                            DateTime dateStart = UnpackDateTime(myReader["Date Start"].ToString());
                            DateTime dateTargetComplete = UnpackDateTime(myReader["Date Target Completion"].ToString());
                            string tags = myReader["Tag List"].ToString();
                            int owner = (int)UnpackLongInt(myReader["FK User Owner ID"].ToString());
                            int capabilityID = (int)UnpackLongInt(myReader["FK Capability ID"].ToString());

                            // Create Feature
                            Feature tFeature = new Feature(id, name, desc, jiraID, state, capFlag, blockedFlag,
                                tShirt, estimatePoints, jiraProjectName, dateCreated, dateLastModified,
                                datePendingApproval, dateReadyToStart, dateInProgress, dateDevComplete,
                                dateTestComplete, dateAccepted, datePortfolioAsk, dateStart, dateTargetComplete,
                                tags, owner, capabilityID);

                            // Add the feature
                            try
                            {
                                port.AddFeature(tFeature);
                            }
                            catch (System.Exception e)
                            {
                                Console.WriteLine(e.ToString());
                            }
                            // Add stubs for Capability and Users
                            port.AddCapabilityStub(capabilityID);
                            port.AddUserStub(owner);

                            // Add the (parent, child) relationship to featureCapabilityKids
                            capabilityFeatureKids.Add(new Tuple<int, int>(capabilityID, id));
                        }
                        myReader.Close();
                    }
                    Console.WriteLine("Features Read: {0}", itemCount);
                    itemCount = 0;
                    Console.WriteLine("Reading Capabilities at: {0}", DateTime.Now.ToString());

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read Feature Acceptance Criteria from AC
                    ///////////////////////////////////////////////////////////////////////////////
                    // First, gather all the feature IDs in a string
                    itemCount = 0;
                    StringBuilder featureKeyString = new StringBuilder("(");
                    bool initial = true;
                    foreach (KeyValuePair<int, Feature> kvp in port._features)
                    {
                        if (initial)
                        {
                            initial = false;
                        }
                        else
                        {
                            featureKeyString.Append(",");
                        }
                        featureKeyString.Append(kvp.Value._id.ToString());
                    }
                    featureKeyString.Append(")");

                    cmdString = "SELECT [Feature Acceptance Criteria Name], [FK Feature ID], [Feature Acceptance Criteria ID] " +
                        " FROM rsd.ACFeatureAcceptanceCriteria WHERE [FK Feature ID] in " +
                        featureKeyString.ToString() + " ORDER BY [FK Feature ID]";
                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        StringBuilder acceptanceText = new StringBuilder();
                        int currentFeature = -1;
                        while (myReader.Read())
                        {
                            itemCount++;
                            // Read the two fields
                            string text = myReader["Feature Acceptance Criteria Name"].ToString();
                            int acID = (int)UnpackLongInt(myReader["Feature Acceptance Criteria ID"].ToString());
                            int featureID = (int)UnpackLongInt(myReader["FK Feature ID"].ToString());

                            // Are we adding to a feature we've already seen?
                            if (currentFeature < 0)
                            {
                                // First line in the results set
                                acceptanceText.Append("[AC ID:" + acID.ToString() + "]" + text);
                                currentFeature = featureID;
                            }
                            else if (currentFeature == featureID)
                            {
                                // Continuation state
                                acceptanceText.Append("<p>" + "[AC ID:" + acID.ToString() + "]" + text);
                            }
                            else
                            {
                                if (acceptanceText.Length > 0)
                                {
                                    if (port._features.ContainsKey(currentFeature) != true)
                                    {
                                        Console.WriteLine("Feature should be present!!");
                                    }
                                    else
                                    {
                                        port._features[currentFeature]._acceptanceCriteria = acceptanceText.ToString();
                                    }
                                }
                                acceptanceText.Clear();
                                currentFeature = featureID;
                            }

                        }
                        myReader.Close();
                        port._features[currentFeature]._acceptanceCriteria = acceptanceText.ToString();

                    }
                    Console.WriteLine("Feature Acceptance Records Read: {0}", itemCount);

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the Capabilties
                    ///////////////////////////////////////////////////////////////////////////////
                    cmdString = "SELECT [Capability ID],[Capability Name],[Capability Description],[Capability State],[External Capability ID]," +
                        "[Budget],[Total CapEx],[External CapEx],[Total OpEx],[External OpEx],[Date Targeted],[Date Started],[Date Portfolio Ask]," +
                        "[Date Created],[Date Last Modified],[Date In Progress],[Date Accepted],[FK User Owner ID],[FK Product Bundle ID]," +
                        "rsd.ACTagList.[Tag List]" +
                        "FROM rsd.ACCapability LEFT JOIN rsd.ACTagList " +
                        "ON rsd.ACCapability.[FK Tag List ID] = rsd.ACTagList.[FK Tag List ID] " +
                        "WHERE [Capability ID] in " + port.GenerateCapabilityList();

                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;
                            // Read Capability Fields
                            int id = (int)UnpackLongInt(myReader["Capability ID"].ToString());
                            string name = myReader["Capability Name"].ToString();
                            string desc = myReader["Capability Description"].ToString();
                            ACIssue.ACIssueState state = ACIssue.StringToState(myReader["Capability State"].ToString());
                            string extID = myReader["External Capability ID"].ToString();
                            double budget = UnpackDouble(myReader["Budget"].ToString());
                            double totCapX = UnpackDouble(myReader["Total CapEx"].ToString());
                            double extCapX = UnpackDouble(myReader["External CapEx"].ToString());
                            double totOpX = UnpackDouble(myReader["Total OpEx"].ToString());
                            double extOpX = UnpackDouble(myReader["External OpEx"].ToString());
                            DateTime dateTargeted = UnpackDateTime(myReader["Date Targeted"].ToString());
                            DateTime dateStarted = UnpackDateTime(myReader["Date Started"].ToString());
                            DateTime datePortfolioAsk = UnpackDateTime(myReader["Date Portfolio Ask"].ToString());
                            DateTime dateCreated = UnpackDateTime(myReader["Date Created"].ToString());
                            DateTime dateLastModified = UnpackDateTime(myReader["Date Last Modified"].ToString());
                            DateTime dateInProgress = UnpackDateTime(myReader["Date In Progress"].ToString());
                            DateTime dateAccepted = UnpackDateTime(myReader["Date Accepted"].ToString());
                            int owner = (int)UnpackLongInt(myReader["FK User Owner ID"].ToString());
                            int parent = (int)UnpackLongInt(myReader["FK Product Bundle ID"].ToString());
                            string tags = myReader["Tag List"].ToString();

                            // Create Capability
                            Capability tCapability = new Capability(id, name, desc, owner, tags, parent, state, extID, budget, totCapX, extCapX, totOpX,
                                extOpX, dateTargeted, dateStarted, datePortfolioAsk, dateCreated, dateLastModified, dateInProgress, dateAccepted);
                            pbCapabilityKids.Add(new Tuple<int, int>(parent, id));
                            
                            // Add the children to this capability
                            foreach (Tuple<int, int> t in capabilityFeatureKids)
                            {
                                if (t.Item1 == id)
                                {
                                    tCapability.AddChild(t.Item2);
                                    tCapability.AddChildObject(port._features[t.Item2]);
                                }
                            }

                            // Update the Capability and stub associated Users and Product Bundle
                            port.UpdateCapability(tCapability);
                            port.AddProductBundleStub(parent);
                            port.AddUserStub(owner);
                        }
                        myReader.Close();
                    }
                    Console.WriteLine("Capabilities Read: {0}", itemCount);
                    itemCount = 0;
                    Console.WriteLine("Reading Product Bundles at: {0}", DateTime.Now.ToString());

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the Product Bundles
                    ///////////////////////////////////////////////////////////////////////////////
                    cmdString = "SELECT [Product Bundle ID],[Product Bundle Name],[Product Bundle Description],[Developmental Step],[Product Bundle State]," +
                        "[Business Impact],[Risk Appetite],[IT Risk],[Failure Impact],[Failure Probability],[Date Target],[Date Start],[Date Portfolio Ask]," +
                        "[Date Created],[Date Last Modified],[Date In Progress],[Date Accepted],[FK UserOwner ID],[FK Concept ID]," +
                        "rsd.ACTagList.[Tag List]" +
                        "FROM rsd.ACProductBundle LEFT JOIN rsd.ACTagList " +
                        "ON rsd.ACProductBundle.[FK Tag List ID] = rsd.ACTagList.[FK Tag List ID] " +
                        "WHERE [Product Bundle ID] in " + port.GeneratePBList();

                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;

                            // Read Product Bundle Fields
                            int id = (int)UnpackLongInt(myReader["Product Bundle ID"].ToString());
                            string name = myReader["Product Bundle Name"].ToString();
                            string desc = myReader["Product Bundle Description"].ToString();
                            string developmentalStep = myReader["Developmental Step"].ToString();
                            ACIssue.ACIssueState state = ACIssue.StringToState(myReader["Product Bundle State"].ToString());
                            string businessImpact = myReader["Business Impact"].ToString();
                            string riskAppetite = myReader["Risk Appetite"].ToString();
                            string itRisk = myReader["IT Risk"].ToString();
                            string failureImpact = myReader["Failure Impact"].ToString();
                            string failureProbability = myReader["Failure Probability"].ToString();
                            DateTime dateTarget = UnpackDateTime(myReader["Date Target"].ToString());
                            DateTime dateStarted = UnpackDateTime(myReader["Date Start"].ToString());
                            DateTime datePortfolioAsk = UnpackDateTime(myReader["Date Portfolio Ask"].ToString());
                            DateTime dateCreated = UnpackDateTime(myReader["Date Created"].ToString());
                            DateTime dateLastModified = UnpackDateTime(myReader["Date Last Modified"].ToString());
                            DateTime dateInProgress = UnpackDateTime(myReader["Date In Progress"].ToString());
                            DateTime dateAccepted = UnpackDateTime(myReader["Date Accepted"].ToString());
                            int owner = (int)UnpackLongInt(myReader["FK UserOwner ID"].ToString());
                            int parent = (int)UnpackLongInt(myReader["FK Concept ID"].ToString());
                            string tags = myReader["Tag List"].ToString();

                            // Create Product Bundle
                            ProductBundle tProductBundle = new ProductBundle(id, name, desc, owner, tags, parent, developmentalStep, state, businessImpact,
                                riskAppetite, itRisk, failureImpact, failureProbability, dateTarget, dateStarted, datePortfolioAsk, dateCreated,
                                dateLastModified, dateInProgress, dateAccepted);
                            conceptPBKids.Add(new Tuple<int, int>(parent, id));

                            // Add the children to this capability
                            foreach (Tuple<int, int> t in pbCapabilityKids)
                            {
                                if (t.Item1 == id)
                                {
                                    tProductBundle.AddChild(t.Item2);
                                    tProductBundle.AddChildObject(port._capabilities[t.Item2]);
                                }
                            }

                            // Add the Product Bundle and stub associated Concept and Users
                            port.UpdateProductBundle(tProductBundle);
                            port.AddConceptStub(parent);
                            port.AddUserStub(owner);
                        }
                        myReader.Close();

                    }
                    Console.WriteLine("Product Bundles Read: {0}", itemCount);
                    itemCount = 0;
                    Console.WriteLine("Reading Concepts at: {0}", DateTime.Now.ToString());

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the Concepts
                    ///////////////////////////////////////////////////////////////////////////////
                    cmdString = "SELECT [Concept ID],[Concept Name],[Concept Description],[Concept State],[Concept Planned Budget],[Active Flag]," +
                        "[Developmental Step],[Operational Step],[Rank Global],[Date Start],[Date Target Completion],[Date Portfolio Ask],[Date Created]," +
                        "[Date Last Modified],[FK LOB ID] " +
                        "FROM rsd.ACConcept WHERE [Concept ID] in " + port.GenerateConceptList();


                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;

                            // Read Concept Fields
                            int id = (int)UnpackLongInt(myReader["Concept ID"].ToString());
                            string name = myReader["Concept Name"].ToString();
                            string desc = myReader["Concept Description"].ToString();
                            ACIssue.ACIssueState state = ACIssue.StringToState(myReader["Concept State"].ToString());
                            double budget = UnpackDouble(myReader["Concept Planned Budget"].ToString());
                            string activeFlag = myReader["Active Flag"].ToString();
                            string developmentalStep = myReader["Developmental Step"].ToString();
                            string operationalStep = myReader["Operational Step"].ToString();
                            int rankGlobal = (int)UnpackLongInt(myReader["Rank Global"].ToString());
                            DateTime dateStarted = UnpackDateTime(myReader["Date Start"].ToString());
                            DateTime dateTarget = UnpackDateTime(myReader["Date Target Completion"].ToString());
                            DateTime datePortfolioAsk = UnpackDateTime(myReader["Date Portfolio Ask"].ToString());
                            DateTime dateCreated = UnpackDateTime(myReader["Date Created"].ToString());
                            DateTime dateLastModified = UnpackDateTime(myReader["Date Last Modified"].ToString());
                            int owner = (int)UnpackLongInt(myReader["FK LOB ID"].ToString());
                            string tags = "";
                            // Create Concept
                            Concept tConcept = new Concept(id, name, desc, owner, tags, state, budget, activeFlag, developmentalStep, operationalStep,
                                rankGlobal, dateStarted, dateTarget, datePortfolioAsk, dateCreated, dateLastModified);

                            // Add the children to this capability
                            foreach (Tuple<int, int> t in conceptPBKids)
                            {
                                if (t.Item1 == id)
                                {
                                    tConcept.AddChild(t.Item2);
                                    tConcept.AddChildObject(port._productBundles[t.Item2]);
                                }
                            }

                            // Add the Concept and stub associated Users
                            port.UpdateConcept(tConcept);
                            port.AddUserStub(owner);
                        }
                        myReader.Close();
                    }
                    Console.WriteLine("Concepts Read: {0}", itemCount);
                    itemCount = 0;
                    Console.WriteLine("Reading Users at: {0}", DateTime.Now.ToString());

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the Users
                    ///////////////////////////////////////////////////////////////////////////////
                    cmdString = "SELECT [JiraUserKey],[User ID],[Email],[First Name],[Last Name],[Full Name],[External ID],[User Title],[User Region],[User City],[User Active Flag] " +
                        "FROM rsd.ACUser WHERE [User ID] in " + port.GenerateUserList();

                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;

                            // Read User Fields
                            int id = (int)UnpackLongInt(myReader["User ID"].ToString());
                            string email = myReader["Email"].ToString();
                            string firstName = myReader["First Name"].ToString();
                            string lastName = myReader["Last Name"].ToString();
                            string fullName = myReader["Full Name"].ToString();
                            string extID = myReader["External ID"].ToString();
                            string title = myReader["User Title"].ToString();
                            string region = myReader["User Region"].ToString();
                            string city = myReader["User City"].ToString();
                            bool isActive = UnpackBool(myReader["User Active Flag"].ToString());
                            string ntid = myReader["JiraUserKey"].ToString();

                            // Create User
                            User tUser = new User(id, email, firstName, lastName, fullName, extID, title, region, city, isActive, ntid);

                            // Add the Concept and stub associated Users
                            port.UpdateUser(tUser);
                        }
                        myReader.Close();
                        Console.WriteLine("Users Read: {0}", itemCount);
                        Console.WriteLine("Done reading Portfolios at: {0}", DateTime.Now.ToString());
                    }

                    sqlConn.Close();

                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }


        public static string StripNonPrintingChars(string input)
        {
            return input.Replace('\t', ' ').Replace('\r', ' ').Replace('\n', ' ').Replace("  ", " ");
        }

        public static void ReadJiraData(Dictionary<string, string> configKeys, string account, string pwd, Portfolio port, bool readUsers = false)
        {
            string dbConnString = @"Data Source=devsecops-dev.database.windows.net;Initial Catalog=devteqimpwu2sdlcstore-sqldb0;Persist Security Info=True;User ID="+account+";Password=" + pwd + @";Authentication=""Active Directory Password"";Connect Timeout=300";

            // TODO - recode this to TEAMS when that column exists in Data Lake for Jira
            if (!configKeys.ContainsKey("PROJECTS"))
            {
                Console.WriteLine("PROJECTS key not specified.  Please rerun and specify this key in the format '<jiraproj1>'[,<jiraproj2>]*");
                return;
            }

            Console.WriteLine("Reading Jira Items at: {0}", DateTime.Now.ToString());
            Console.WriteLine("Reading from Issue table");
            ///////////////////////////////////////////////////////////////////////////////
            // Read the Jira Items
            ///////////////////////////////////////////////////////////////////////////////
            int itemCount = 0;
            using (SqlConnection sqlConn = new SqlConnection(dbConnString))
            {
                string cmdString = "";
                try
                {
                    sqlConn.Open();
                    SqlDataReader myReader = null;

                    // Fetch Jira Items - ORDER BY is important so that parent creation is guaranteed
                    cmdString = "SELECT JiraID, JiraKey, Type, Priority, FeatureJiraKey, ParentJiraID, Created, Creator," +
                        " Assignee, Severity, CreatorNTID, CreatorEmail, UpdatedOnDate, AssigneeNTID, AssigneeEmail, " +
                        " DeliveryTeam, Component, Director, Summary, AcceptanceCriteria, Description, AppSupportDirector, SolutionDirector, " +
                        " SolutionVP, PhaseDetected, Channel, EstimatedBuildDate, Status," +
                        " RemainingEstimate, OriginalEstimate, AggregateTimeSpent, Sprints, Points, Labels, ReleaseProject, ETAForFix " +
                        " FROM rsd.JiraSummary join rsd.JiraProject on JiraSummary.ProjectID = JiraProject.JiraProjectID " +
                        " WHERE JiraProjectKey IN (" + configKeys["TEAMS"] + ") ORDER BY JiraID";

                    // Read all Issues
                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;
                            int jiraID = (int)UnpackLongInt(myReader["JiraID"].ToString());
                            string jiraKey = myReader["JiraKey"].ToString();
                            string issueType = myReader["Type"].ToString();
                            string status = myReader["Status"].ToString();
                            string priority = myReader["Priority"].ToString();
                            string featureKey = myReader["FeatureJiraKey"].ToString();
                            int parentID = (int)UnpackLongInt(myReader["ParentJiraID"].ToString());
                            DateTime created = UnpackDateTime(myReader["Created"].ToString());
                            string creator = myReader["Creator"].ToString();
                            string assignee = myReader["Assignee"].ToString();
                            string severity = myReader["Severity"].ToString();
                            string creatorNTID = myReader["CreatorNTID"].ToString();
                            string creatorEmail = myReader["CreatorEmail"].ToString();
                            DateTime dateModifiedJira = UnpackDateTime(myReader["UpdatedOnDate"].ToString());
                            string assigneeNTID = myReader["AssigneeNTID"].ToString();
                            string assigneeEmail = myReader["AssigneeEmail"].ToString();
                            string deliveryTeam = myReader["DeliveryTeam"].ToString();
                            string component = myReader["Component"].ToString();
                            string director = myReader["Director"].ToString();
                            string summary = myReader["Summary"].ToString();
                            string acceptanceCriteria = myReader["AcceptanceCriteria"].ToString();
                            string description = myReader["Description"].ToString();
                            string directorAppSupport = myReader["AppSupportDirector"].ToString();
                            string directorSolution = myReader["SolutionDirector"].ToString();
                            string vp = myReader["SolutionVP"].ToString();
                            string phase = myReader["PhaseDetected"].ToString();
                            DateTime estBuildDate = UnpackDateTime(myReader["EstimatedBuildDate"].ToString());
                            double remainingEstimate = UnpackDouble(myReader["RemainingEstimate"].ToString());
                            double origEstimate = UnpackDouble(myReader["OriginalEstimate"].ToString());
                            double aggTimeSpent = UnpackDouble(myReader["AggregateTimeSpent"].ToString());
                            string sprint = myReader["Sprints"].ToString();
                            double storyPoints = UnpackDouble(myReader["Points"].ToString());
                            string labels = myReader["Labels"].ToString();
                            string releaseProject = myReader["ReleaseProject"].ToString();
                            DateTime estFixDate = UnpackDateTime(myReader["ETAForFix"].ToString());

                            // Based on issue type, store in the proper list
                            switch (issueType)
                            {
                                case "Feature": // We already should have the feature from AC, add these fields
                                    if (port._jiraFeats.ContainsKey(jiraKey))
                                    {
                                        // Agilecraft created this, blend the data
                                        port._jiraFeats[jiraKey]._jiraRowID = jiraID; // Needed if subtasks are attached to the feature directly
                                        port._jiraFeats[jiraKey].UpdateFeatureFromJira(priority, featureKey, creator, creatorNTID, creatorEmail, severity,
                                           assignee, assigneeNTID, assigneeEmail, deliveryTeam, component, director, acceptanceCriteria, description,
                                           directorAppSupport, directorSolution, vp, phase, estBuildDate, remainingEstimate, origEstimate, aggTimeSpent,
                                           sprint, storyPoints, labels, releaseProject, estFixDate, dateModifiedJira);
                                    }
                                    else
                                    {
                                        // No prior feature in the AgileCraft data, make a new feature
                                        DateTime unknownDate = new DateTime(1900, 1, 1);
                                        string projectKey = jiraKey.Substring(0, jiraKey.IndexOf('-'));
                                        Feature tFeature = new Feature(jiraID, summary, description, jiraKey, ACIssue.ACIssueState.Unknown,
                                            "", "", "", storyPoints, projectKey, created, dateModifiedJira, unknownDate, unknownDate,
                                            unknownDate, unknownDate, unknownDate, unknownDate, unknownDate, unknownDate, unknownDate,
                                            labels, 0, 0);
                                        tFeature._jiraOnly = true;
                                        port.AddFeature(tFeature);
                                        tFeature.UpdateFeatureFromJira(priority, featureKey, creator, creatorNTID, creatorEmail, severity,
                                            assignee, assigneeNTID, assigneeEmail, deliveryTeam, component, director, acceptanceCriteria, description,
                                            directorAppSupport, directorSolution, vp, phase, estBuildDate, remainingEstimate, origEstimate, aggTimeSpent,
                                            sprint, storyPoints, labels, releaseProject, estFixDate, dateModifiedJira);
                                    }
                                    break;
                                case "Bug": //New story based on JiraID (int), add it
                                case "Story": //New story based on JiraID (int), add it
                                    Story tStory = new Story(jiraID, jiraKey, featureKey, status, priority, parentID, summary, labels, created,
                                        creator, assignee, severity, creatorNTID, creatorEmail, dateModifiedJira, assigneeNTID, assigneeEmail,
                                        deliveryTeam, component, director, acceptanceCriteria, description, directorAppSupport, directorSolution,
                                        vp, phase, estBuildDate, remainingEstimate, origEstimate, aggTimeSpent, sprint, storyPoints, releaseProject,
                                        estFixDate, issueType);
                                    port.AddStory(tStory);
                                    break;
                                case "Sub-task": // New task based on JiraID (int), add it

                                    Task tTask = new Task(jiraID, jiraKey, status, priority, parentID, summary, labels, created,
                                        creator, assignee, severity, creatorNTID, creatorEmail, dateModifiedJira, assigneeNTID, assigneeEmail,
                                        deliveryTeam, component, director, acceptanceCriteria, description, directorAppSupport, directorSolution,
                                        vp, phase, estBuildDate, remainingEstimate, origEstimate, aggTimeSpent, sprint, storyPoints, releaseProject,
                                        estFixDate);
                                    // Safety check for parentless items

                                    port.AddTask(tTask);
                                    break;
                            }

                        }
                        myReader.Close();
                        Console.WriteLine();
                        Console.WriteLine("Jira Issues Read: {0}", itemCount);
                        Console.WriteLine("Done reading Jira Issues at: {0}", DateTime.Now.ToString());
                        Console.WriteLine("Matching parent issues");

                        // After we've read all of the items, THEN do the parent identification logic
                        // For stories, match _jiraFeatureKey _jiraKey of features
                        foreach (KeyValuePair<int, Story> kvp in port._stories)
                        {
                            Story s = kvp.Value;
                            if (port._jiraFeats.ContainsKey(s._jiraFeatureKey))
                            {
                                Feature f = port._jiraFeats[s._jiraFeatureKey];
                                s._parentID = f._id;
                                f.AddChild(s._id);
                                f.AddChildObject(s);
                            }
                            else if (s._issueType != ACIssue.ACIssueType.Bug)
                            {
                                port.AddNag(Nag.NagVerb.E_Issue_IsOrphaned, "Parent feature '"+s._jiraFeatureKey +"' not found", s._jiraKey, s._jiraAssignee, s.JiraUrl());
                            }
                        }

                        Console.WriteLine("Done matching parent issues");
                        Console.WriteLine("Aligning tasks");

                        // For tasks, the parent link is via _parentID, not _jiraFeatureKey, so the lookup is harder
                        foreach (KeyValuePair<int, Task> kvp in port._tasks)
                        {
                            Task t = kvp.Value;

                            ACIssue parent = null;
                            if (port._stories.ContainsKey(t._parentID))
                            {
                                parent = port._stories[t._parentID];
                                parent.AddChild(t._id);
                                parent.AddChildObject(t);
                            }
                            else if (port._features.ContainsKey(t._parentID))
                            {
                                parent = port._features[t._parentID];
                                parent.AddChild(t._id);
                                parent.AddChildObject(t);
                            }
                            else
                            {
                                port.AddNag(Nag.NagVerb.E_Issue_IsOrphaned, "No parent found for task", t._jiraKey, t._jiraAssignee, t.JiraUrl());
                            }

                        }
                        Console.WriteLine("Done aligning tasks");

                    }

                    Console.WriteLine("Reading worklogs");

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the WorkLogs
                    ///////////////////////////////////////////////////////////////////////////////
                    cmdString = "SELECT ID, JiraID, Author, WorklogBody, Created, StartDate, Timeworked from rsd.JiraWorklog where JiraID in "
                        + port.GenerateSubTaskList();
                    itemCount = 0;
                    // Read all WorkLogs
                    using (SqlCommand myCommand = new SqlCommand(cmdString))
                    {
                        myCommand.CommandTimeout = 0;
                        myCommand.Connection = sqlConn;
                        myReader = myCommand.ExecuteReader();
                        while (myReader.Read())
                        {
                            itemCount++;
                            int id = (int)UnpackLongInt(myReader["ID"].ToString());
                            int jiraID = (int)UnpackLongInt(myReader["JiraID"].ToString());
                            string author = myReader["Author"].ToString();
                            string logBody = myReader["WorklogBody"].ToString();
                            DateTime created = UnpackDateTime(myReader["Created"].ToString());
                            DateTime startDate = UnpackDateTime(myReader["StartDate"].ToString());
                            int timeWorked = (int)UnpackLongInt(myReader["Timeworked"].ToString());

                            Worklog workLog = new Worklog(id, jiraID, author, logBody, created, timeWorked);
                            port.AddWorkLog(workLog);
                        }
                    }
                    myReader.Close();
                    Console.WriteLine("Jira Worklogs Read: {0}", itemCount);
                    Console.WriteLine("Done reading Worklogs at: {0}", DateTime.Now.ToString());
                    Console.WriteLine("Rolling up worklogs");
                    port.InitializeAndRollUpWorklogs();
                    Console.WriteLine("Done rolling up worklogs");

                    ///////////////////////////////////////////////////////////////////////////////
                    // Read the Users
                    ///////////////////////////////////////////////////////////////////////////////
                    if (readUsers)
                    {
                        Console.WriteLine("Reading users");
                        port.AddAllJiraUsers();
                        cmdString = "SELECT [Name],[Email],[DisplayName] " +
                            "FROM rsd.JiraUser WHERE [Name] in " + port.GenerateUserList();
                         using (SqlCommand myCommand = new SqlCommand(cmdString))
                        {
                            myCommand.CommandTimeout = 0;
                            myCommand.Connection = sqlConn;
                            myReader = myCommand.ExecuteReader();
                            while (myReader.Read())
                            {
                                itemCount++;

                                // Read User Fields
                                string ntid = myReader["Name"].ToString();
                                string email = myReader["Email"].ToString();
                                string fullName = myReader["DisplayName"].ToString();
                                // Update User
                                if (port._usersByNTID.ContainsKey(ntid))
                                {
                                    port._usersByNTID[ntid].UpdateUser(ntid, email, fullName);
                                }
                                else
                                {
                                    User u = new User(fullName, email, ntid);
                                    port.AddUser(u);
                                }
                            }
                            myReader.Close();
                            Console.WriteLine("Users Read: {0}", itemCount);
                        }
                        Console.WriteLine("Done reading users");
                    }
                }
                catch (System.Exception e)
                {
                     Console.WriteLine(e.ToString());
                }
            }
            Console.WriteLine("Done reading Jira Data at: {0}", DateTime.Now.ToString());

        }

        public static string GenerateHtmlTdForString(string input)
        {
            if (input.CompareTo("Unknown") == 0)
            {
                return string.Format("<td bgcolor=yellow>*** {0}</td>", input);
            }
            return string.Format("<td>{0}</td>", input);
        }
        public static string GenerateHoursFromSeconds(double input)
        {
            if ( input < 0)
            {
                return string.Format("<td>N/A</td>");
            }
            if ( input < 3600)
            {
                return string.Format("<td>{0}m</td>", input / 60.0);
            }
            return string.Format("<td>{0}h</td>", input/3600.0);
        }

        public static string GenerateHtmlTdForDate(DateTime input, bool flagIfUndefined = false)
        {
            if (input.Year == 1900)
            {
                if (flagIfUndefined)
                {
                    return string.Format("<td bgcolor=yellow><center>*** --</center></td>", PrettyPrintDate(input));
                }
                else
                {
                    return string.Format("<td><center>--</center></td>", PrettyPrintDate(input));
                }
            }
            return string.Format("<td>{0}</td>", PrettyPrintDate(input));
        }


        /// This is old Jira REST API work - keep it in case the REST API is reopened
        /// 

        // public static string GetAllIssuesForProjectSet(string pwd, Dictionary<string, string> configKeys)
        // {
        //     string authToken = configKeys["AUTHTOKEN"];
        //     string projectSet = configKeys["TEAMS"];
        //     string requestString = "https://jirasw.t-mobile.com/rest/api/2/search/?";
        //     string jqlString = System.Web.HttpUtility.UrlEncode("project in ( " + projectSet + " )");
        //     requestString += "jql=" + jqlString;
        //     requestString += "&maxResults=500";
        //     return GenerateResponseString(requestString, authToken);
        // }

        //public  static void ParseIssues(string responseString, Dictionary<string, JiraIssue> issues)
        // {
        //     JObject jResults = JObject.Parse(responseString);
        //     IList<JToken> jResultsItems = jResults["issues"].Children().ToList();
        //     for (int i = 0; i < jResultsItems.Count(); i++)
        //     {
        //         JiraIssue issue = new JiraIssue(jResults["issues"][i]);
        //         issues.Add(issue.Key, issue);
        //     }
        // }


        //internal static void ReadJiraData(Dictionary<string, string> configKeys, string pwd, Portfolio port)
        //{
        //    string responseString = "";

        //    responseString = GetAllIssuesForProjectSet(pwd, configKeys);
        //    ParseIssues(responseString, port);

        //    port.MatchJiraChildren();
        //}

        //public static void LoadAllWorklogs(Dictionary<string, JiraIssue> issues, Dictionary<long, Worklog> workLogs, string authToken)
        //{
        //    string responseString = "";

        //    // For each issue I
        //    foreach (KeyValuePair<string, JiraIssue> kvp in issues)
        //    {
        //        JiraIssue thisIssue = kvp.Value;
        //        //   If there's any logged time
        //        if (thisIssue._totalLoggedTime > 0)
        //        {
        //            responseString = FetchWorklog(authToken, thisIssue.Key);
        //            InitializeWorklogs(responseString, workLogs, thisIssue);
        //        }
        //    }
        //}

        //public static void InitializeWorklogs(string responseString, Dictionary<long, Worklog> workLogs, JiraIssue tIssue)
        //{
        //    JObject jResults = JObject.Parse(responseString);
        //    IList<JToken> jResultsItems = jResults["worklogs"].Children().ToList();
        //    tIssue._workLogs.Clear();
        //    for (int i = 0; i < jResultsItems.Count(); i++)
        //    {
        //        Worklog w = new Worklog(jResults["worklogs"][i], tIssue.Key);
        //        workLogs.Add(w._id, w);
        //        tIssue._workLogs.Add(w);
        //    }
        //}

        //public static string FetchWorklog(string authToken, string taskKey)
        //{
        //    string requestString = " https://jirasw.t-mobile.com/rest/api/2/issue/" + taskKey + "/worklog";
        //    return GenerateResponseString(requestString, authToken);
        //}




    }
}
